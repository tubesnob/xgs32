/*

   COPYRIGHT NOTIFICATION

   This information is copyrighted by
   Steven W. Mentzer

   You may not sell or assume ownership of this information 
   without my express written consent.

   This information can be freely redistributed, published, compiled, used or modified.

   Inquiries should be directed to   steve@staticmonkey.com

*/

#include "vid-drv_pixel.h"
#include "xgstypes.h"
#include "vid-drv.h"
#include "hires.h"
#include "video.h"
#include "xgsmemory.h"
#include "Xgsstate.h"
#include "vid-drv_utils.h"

// this is an array of function pointers to routines that plot pixels
// for each bit depth you are running. 
void		(*VID_PixelPlot[10])(long,long,long);


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////



void VID_PP_32_SUPER(long x, long y, long lColorIndex)
{
   long  lPixelColor=LOOKUPSUPERCOLOR24(lColorIndex);
   word32 *lScreen=(word32*) GETSCREENADDR(x,y);
   *lScreen = lPixelColor;
};
void VID_PP_32_BORDER(long x, long y, long lColorIndex)
{
   long  lPixelColor=LOOKUPBORDERCOLOR24(lColorIndex);
   word32 *lScreen=(word32*) GETSCREENADDR(x,y);
   *lScreen = lPixelColor;
};
void VID_PP_32_TEXT(long x, long y, long lColorIndex)
{
   long  lPixelColor=LOOKUPTEXTCOLOR24(lColorIndex);
   word32 *lScreen=(word32*) GETSCREENADDR(x,y);
   *lScreen = lPixelColor;
};
void VID_PP_32_HIRES(long x, long y, long lColorIndex)
{
   long  lPixelColor=LOOKUPTEXTCOLOR24(lColorIndex);
   word32 *lScreen=(word32*) GETSCREENADDR(x,y);
   *lScreen = lPixelColor;
};
void VID_PP_32_DBL(long x, long y, long lColorIndex)
{
   long  lPixelColor=LOOKUPTEXTCOLOR24(vid_dhires_colors[lColorIndex>>8][lColorIndex&0xFF]);
   word32 *lScreen=(word32*) GETSCREENADDR(x,y);
   *lScreen = lPixelColor;
};


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////



void VID_PP_24_SUPER(long x, long y, long lColorIndex)
{
   long     lPixelColor=LOOKUPSUPERCOLOR24(lColorIndex);
   byte     *pScreen = (byte*) GETSCREENADDR(x,y);
   byte     *pColor = (byte*) &lPixelColor;
	*pScreen++ = *pColor++;
	*pScreen++ = *pColor++;
	*pScreen++ = *pColor++;
};
void VID_PP_24_BORDER(long x, long y, long lColorIndex)
{
   long     lPixelColor=LOOKUPBORDERCOLOR24(lColorIndex);
   byte     *pScreen = (byte*) GETSCREENADDR(x,y);
   byte     *pColor = (byte*) &lPixelColor;
	*pScreen++ = *pColor++;
	*pScreen++ = *pColor++;
	*pScreen++ = *pColor++;
};
void VID_PP_24_TEXT(long x, long y, long lColorIndex)
{
   long     lPixelColor=LOOKUPTEXTCOLOR24(lColorIndex);
   byte     *pScreen = (byte*) GETSCREENADDR(x,y);
   byte     *pColor = (byte*) &lPixelColor;
	*pScreen++ = *pColor++;
	*pScreen++ = *pColor++;
	*pScreen++ = *pColor++;
};
void VID_PP_24_HIRES(long x, long y, long lColorIndex)
{
   long     lPixelColor=LOOKUPTEXTCOLOR24(lColorIndex);
   byte     *pScreen = (byte*) GETSCREENADDR(x,y);
   byte     *pColor = (byte*) &lPixelColor;
	*pScreen++ = *pColor++;
	*pScreen++ = *pColor++;
	*pScreen++ = *pColor++;
};
void VID_PP_24_DBL(long x, long y, long lColorIndex)
{
   long     lPixelColor=LOOKUPTEXTCOLOR24(vid_dhires_colors[lColorIndex>>8][lColorIndex&0xFF]);
   byte     *pScreen = (byte*) GETSCREENADDR(x,y);
   byte     *pColor = (byte*) &lPixelColor;
	*pScreen++ = *pColor++;
	*pScreen++ = *pColor++;
	*pScreen++ = *pColor++;
};

///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////


void VID_PP_16_SUPER(long x, long y, long lColorIndex)
{
   word16         iPixelColor=LOOKUPSUPERCOLOR16(lColorIndex);
   word16         *iScreen=(word16*) GETSCREENADDR(x,y);
   *iScreen = iPixelColor;
};

void VID_PP_16_BORDER(long x, long y, long lColorIndex)
{
   word16         iPixelColor=LOOKUPBORDERCOLOR16(lColorIndex);
   word16         *iScreen=(word16*) GETSCREENADDR(x,y);
   *iScreen = iPixelColor;
};

void VID_PP_16_TEXT(long x, long y, long lColorIndex)
{
   word16         iPixelColor=LOOKUPTEXTCOLOR16(lColorIndex);
   word16         *iScreen=(word16*) GETSCREENADDR(x,y);
   *iScreen = iPixelColor;
};
void VID_PP_16_HIRES(long x, long y, long lColorIndex)
{
   word16         iPixelColor=LOOKUPTEXTCOLOR16(lColorIndex);
   word16         *iScreen=(word16*) GETSCREENADDR(x,y);
   *iScreen = iPixelColor;
};
void VID_PP_16_DBL(long x, long y, long lColorIndex)
{
   word16         iPixelColor=LOOKUPTEXTCOLOR16(vid_dhires_colors[lColorIndex>>8][lColorIndex&0xFF]);
   word16         *iScreen=(word16*) GETSCREENADDR(x,y);
   *iScreen = iPixelColor;
};


///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////
///////////////////////////////////////////////////////////////////////


void VID_PP_8_SUPER(long x, long y, long lColorIndex)
{
   byte *pScreen = GETSCREENADDR(x,y);
   *pScreen = (byte) lColorIndex&0xff;
};
void VID_PP_8_BORDER(long x, long y, long lColorIndex)
{
   byte *pScreen = GETSCREENADDR(x,y);
   *pScreen = (byte) lColorIndex&0xff;
};
void VID_PP_8_TEXT(long x, long y, long lColorIndex)
{
   byte *pScreen = GETSCREENADDR(x,y);
   *pScreen = (byte) lColorIndex&0xff;
};
void VID_PP_8_HIRES(long x, long y, long lColorIndex)
{
   byte *pScreen = GETSCREENADDR(x,y);
   *pScreen = (byte) lColorIndex&0xff;
};
void VID_PP_8_DBL(long x, long y, long lColorIndex)
{
   byte *pScreen = GETSCREENADDR(x,y);
   *pScreen = (byte) lColorIndex&0xff;
};


