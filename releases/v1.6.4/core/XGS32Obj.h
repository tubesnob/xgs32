// XGS32Obj.h : Declaration of the CXGS32Obj

#ifndef __XGS32OBJ_H_
#define __XGS32OBJ_H_

#include "resource.h"       // main symbols

#define XGS32_VERSION   "1.6.48"

/////////////////////////////////////////////////////////////////////////////
// CXGS32Obj
class ATL_NO_VTABLE CXGS32Obj : 
	public CComObjectRootEx<CComSingleThreadModel>,
	public CComCoClass<CXGS32Obj, &CLSID_XGS32Obj>,
	public ISupportErrorInfo,
	public IConnectionPointContainerImpl<CXGS32Obj>,
	public IXGS32Obj
{
public:
	CXGS32Obj()
	{
	}

DECLARE_REGISTRY_RESOURCEID(IDR_XGS32OBJ)

DECLARE_PROTECT_FINAL_CONSTRUCT()

BEGIN_COM_MAP(CXGS32Obj)
	COM_INTERFACE_ENTRY(IXGS32Obj)
	COM_INTERFACE_ENTRY(ISupportErrorInfo)
	COM_INTERFACE_ENTRY(IConnectionPointContainer)
END_COM_MAP()
BEGIN_CONNECTION_POINT_MAP(CXGS32Obj)
END_CONNECTION_POINT_MAP()


// ISupportsErrorInfo
	STDMETHOD(InterfaceSupportsErrorInfo)(REFIID riid);

// IXGS32Obj
public:
	STDMETHOD(get_JoystickMode)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_JoystickMode)(/*[in]*/ long newVal);
	STDMETHOD(EmulatorPause)();
	STDMETHOD(get_LocalSpeedIndex)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_EmulatorUpdateMode)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_EmulatorUpdateMode)(/*[in]*/ long newVal);
	STDMETHOD(get_EmulatorUpdatePeriod)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_EmulatorUpdatePeriod)(/*[in]*/ long newVal);
	STDMETHOD(get_Version)(/*[out, retval]*/ BSTR *pVal);


	STDMETHOD(Initialize)();
	STDMETHOD(EmulatorStop)();
	STDMETHOD(EmulatorRun)();
	STDMETHOD(Shutdown)();

	STDMETHOD(get_AverageVideoRefreshRate)(/*[out, retval]*/ double *pVal);
	STDMETHOD(get_TargetVideoRefreshRate)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_TargetVideoRefreshRate)(/*[in]*/ double newVal);
	STDMETHOD(get_LockVideoUpdates)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_LockVideoUpdates)(/*[in]*/ long newVal);
	//STDMETHOD(SetVideoMode)(/*[in]*/ long lX, /*[in]*/ long lY, /*[in]*/ long lBPP, /*[in]*/ long lRefreshRate, long lWindowed);
   STDMETHOD(SetVideoMode)(long lX, long lY, long lBPP, long lRefreshRate, long lWindowed, long lWindowedParentHwnd);
	STDMETHOD(get_ValidDisplayModes)(/*[out, retval]*/ VARIANT *pVal);

	STDMETHOD(get_AttachedJoySticks)(VARIANT *pVal);

	STDMETHOD(get_SoundMode)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_SoundMode)(/*[in]*/ long newVal);
	STDMETHOD(get_SoundOutputBufferSize)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_SoundOutputBufferSize)(/*[in]*/ long newVal);
	STDMETHOD(get_SoundOutputBuffers)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_SoundOutputBuffers)(/*[in]*/ long newVal);

	STDMETHOD(get_RunState)(/*[out, retval]*/ long *pVal);
	STDMETHOD(get_TargetRunSpeed)(/*[out, retval]*/ double *pVal);
	STDMETHOD(put_TargetRunSpeed)(/*[in]*/ double newVal);
	STDMETHOD(get_AverageRunSpeed)(/*[out, retval]*/ double *pVal);

	STDMETHOD(get_Slot)(/*[in]*/ long lSlotNum, /*[in]*/ long lDeviceNum, /*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Slot)(/*[in]*/ long lSlotNum, /*[in]*/ long lDeviceNum, /*[in]*/ BSTR newVal);
	STDMETHOD(get_SmartPort)(/*[in]*/ long lPortNum, /*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_SmartPort)(/*[in]*/ long lPortNum, /*[in]*/ BSTR newVal);
	STDMETHOD(get_BRAMFile)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_BRAMFile)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Font80File)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Font80File)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_Font40File)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_Font40File)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_ROMFile)(/*[out, retval]*/ BSTR *pVal);
	STDMETHOD(put_ROMFile)(/*[in]*/ BSTR newVal);
	STDMETHOD(get_RAMSize)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_RAMSize)(/*[in]*/ long newVal);

	STDMETHOD(get_DebugMode)(/*[out, retval]*/ long *pVal);
	STDMETHOD(put_DebugMode)(/*[in]*/ long newVal);

};

#endif //__XGS32OBJ_H_
