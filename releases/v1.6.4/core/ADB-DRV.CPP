
#include "adb-drv.h"
#include "xgsstate.h"
#include "adb.h"
#include "vid-drv.h"
#include "emul.h"
#include "cpu.h"
#include "xgsdispatch.h"
#include "debugger.h"

void ADB_PauseInput()
{
   ADB_inputShutdown();
};

void ADB_RestoreInputAfterPause()
{
};

__forceinline void ADB_KeyPress(char keycode)
{
	if (keycode) 
   {
		ski_input_buffer[ski_input_index++] = keycode;
		if (ski_input_index == ADB_INPUT_BUFFER) 
         ski_input_index = 0;
	}
};

int ADB_inputInit()
{
   adb_grab_mode=0;
	return 0;
}

void ADB_inputShutdown()
{
}


void ADB_inputKeyDown(int keycode, int isvirt) 
{
   SETFUNCTION("ADB_inputKeyDown");
	POINT	Offset;
	char	keyout;
   short rv;

	if (isvirt) 
   {
		keyout = 0;
		switch(keycode) 
      {
			case VK_LEFT:
				keyout = 0x08;
				break;
			case VK_RIGHT:
				keyout = 0x15;
				break;
			case VK_UP:
				keyout = 0x0B;
				break;
			case VK_DOWN:
				keyout = 0x0A;
				break;

			case VK_CAPITAL:
				ski_modifier_reg |= 0x04;
				break;

			case VK_SHIFT:
				ski_modifier_reg |= 0x01;
				break;

			case VK_CONTROL:
				ski_modifier_reg |= 0x02;
				break;

			case VK_F3:
				ski_modifier_reg |= 0x80;
				break;
			case VK_F4:
				ski_modifier_reg |= 0x40;
				break;

			case VK_F5:

            // only enable the virtual joystick if the regular joystick
            // mode is turned off...
            if (g_lJoystickMode==0)
            {
				   if (adb_grab_mode == 1) 
               {
 					   adb_grab_mode = 0;
 					   ReleaseCapture();
					   ShowCursor(1);
 				   }
               else
               {
                  if (adb_grab_mode==2)
                  {
 					      ReleaseCapture();
					      ShowCursor(1);
                  };
 					   adb_grab_mode = 1;
					   ShowCursor(0);
					   SetCapture(g_hVidHWND);
					   ski_lastx = VID_WIDTH/2;
					   ski_lasty = VID_HEIGHT/2;
					   Offset.x = ski_lastx;
					   Offset.y = ski_lasty;
					   ClientToScreen(g_hVidHWND, &Offset);
					   SetCursorPos(Offset.x, Offset.y);
 				   }			
            };
				break;

			case VK_F6:
				if (adb_grab_mode == 2) 
            {
 					adb_grab_mode = 0;
 					ReleaseCapture();
					ShowCursor(1);
 				} 
            else
            {
               if (adb_grab_mode==1)
               {
 					   ReleaseCapture();
					   ShowCursor(1);
               };
 					adb_grab_mode = 2;
					ShowCursor(0);
					SetCapture(g_hVidHWND);
					ski_lastx = VID_WIDTH/2;
					ski_lasty = VID_HEIGHT/2;
					Offset.x = ski_lastx;
					Offset.y = ski_lasty;
					ClientToScreen(g_hVidHWND, &Offset);
					SetCursorPos(Offset.x, Offset.y);
 				}			
				break;

			case VK_F7:
            if (g_lShowStatus)
            {
               g_lShowStatus=0;
               VID_outputResize(vid_out_width,vid_out_height);
            }
            else
               g_lShowStatus=1;
  				break;

         case VK_F8:
            
            // switch from fullscreen to windowed and vice-versa
            g_lChangeVideoMode=1;
            break;

         case VK_F9:

               static long previousdebugmode;
               if (g_lDebugMode)
               {
                  if (g_lDebugMode==2)
                  {
                     DEBUGGER_Stop();
                     g_lDebugMode=previousdebugmode;
                  }
                  else
                  {
                     previousdebugmode=g_lDebugMode;
                     g_lDebugMode=2;
                     DEBUGGER_Start();
                     g_lDebugHalt=1;
                      
                  };
               };
               break;

         case VK_PAUSE:
            if (g_lDebugMode)
            {
               if (g_lDebugRun==1)
               {
                  g_lDebugRun=0;
                  g_lDebugHalt=1;
               }
               else
                  g_lDebugHalt=0;
            };
            break;

         case VK_INSERT:
            if (g_lDebugMode)
            {
               g_lDebugRun=1;
               g_lDebugHalt=0;
            };
            break;


			case VK_F12:
            rv=GetKeyState(VK_CONTROL);
            if ((rv&0x80)==0x80)
               g_lEmulatorSignal=EMUL_DOQUIT;
  				break;

			case VK_F11:
				g_lEmulatorSignal=EMUL_DOPAUSE;
				break;

			case VK_HOME:
				if (ski_modifier_reg & 0x02) EMUL_reset();
				break;

			case VK_F1:
				if ((ski_modifier_reg & 0x82) == 0x82) 
            {
					if (!ski_status_irq) 
               {
						ski_status_irq = 0x20;
						keycode = 0x1B;
						CPU_addIRQ();
					}
				} 
            else 
            {
					keyout = 0x1B;
				}
				break;

			default:
				break;
		}
	} 
   else 
   {
		keyout = keycode & 0x7F;
	}

	if (keyout) 
   {
		ski_input_buffer[ski_input_index++] = keyout;
		if (ski_input_index == ADB_INPUT_BUFFER) ski_input_index = 0;
	}
}

void ADB_inputKeyRelease(int keycode, int isvirt) 
{
	if (isvirt) 
   {
		switch(keycode) 
      {
		 	case VK_CAPITAL:
 				ski_modifier_reg &= ~0x04;
				break;
	 		case VK_SHIFT:
 				ski_modifier_reg &= ~0x01;
				break;
			case VK_CONTROL:
 				ski_modifier_reg &= ~0x02;
	 			break;
			case VK_F3:
				ski_modifier_reg &= ~0x80;
				break;
			case VK_F4:
				ski_modifier_reg &= ~0x40;
				break;
		}
	}
}


void ADB_inputUpdate()
{

   JOYINFO  joyinfo;
   HRESULT hr;

   static int button1_down;
   static int button2_down;

   SETFUNCTION("ADB_inputUpdate");

   // update the joystick position...
   if (g_lJoystickMode)
   {
      hr=joyGetPos(JOYSTICKID1,&joyinfo);

      if FAILED(hr)
      {
         switch(hr)
         {
            case MMSYSERR_NODRIVER:
               WriteDebug("No Joystick Driver Loaded...[MMSYSERR_NODRIVER]");
               break;
            case MMSYSERR_INVALPARAM:
               WriteDebug("Invalid Parameter Retrieving Joystick Position [MMSYSERR_INVALPARAM]..");
               break;
            case JOYERR_UNPLUGGED:
               WriteDebug("Joystick Appears To Be Unplugged! [JOYERR_UNPLUGGED]");
               break;
            default:
               sprintf(emul_buffer,"Unspecified Error Retrieving Joystick Position (err=0x%xd)",hr);
               WriteDebug(emul_buffer);
               break;
         }
      }
      else
      {
         //sprintf(emul_buffer,"x=%xd   y=%xd",joyinfo.wXpos,joyinfo.wYpos);
         //WriteDebug(emul_buffer);

         adb_pdl0=(joyinfo.wXpos>>8);
         adb_pdl1=(joyinfo.wYpos>>8);

         if (joyinfo.wButtons&JOY_BUTTON1)
         {
            ski_modifier_reg |= 0x80;      
            button1_down=1;
         }
         else
         {
            if (button1_down)
            {
               ski_modifier_reg &= ~0x80;
               button1_down=0;
            };
         };

         if (joyinfo.wButtons&JOY_BUTTON2)
         {
            ski_modifier_reg |= 0x40;
            button2_down=1;
         }
         else
         {
            if (button2_down)
            {
               ski_modifier_reg &= ~0x40;
               button2_down=0;
            };
         };
      };

   };
}

void ADB_inputMotionNotify(int X, int Y) 
{
	POINT    Offset;

	if (adb_grab_mode == 1)
   {

      if (X<0) X=0;
      if (X> g_lVidWidth) X=g_lVidWidth;
      if (Y<0) Y=0;
      if (Y> g_lVidHeight) Y=g_lVidHeight;

      adb_pdl0 = (X/(g_lVidWidth/256));
      adb_pdl1 = (Y/(g_lVidHeight/256));

      if (adb_pdl0<0)
         adb_pdl0 = 0;
      if (adb_pdl0>254)
         adb_pdl0=254;

      if (adb_pdl1<0)
         adb_pdl1 = 0;
      if (adb_pdl1>254)
         adb_pdl1=254;

	} 
   else if (adb_grab_mode == 2) 
   {
		if (ski_status_reg & 0x80) return;	// Mouse reg still full
 		ski_xdelta = X - ski_lastx;
 		ski_ydelta = Y - ski_lasty;
		Offset.x = ski_lastx;
		Offset.y = ski_lasty;
		ClientToScreen(g_hVidHWND, &Offset);
		if (ski_xdelta || ski_ydelta)
			SetCursorPos(Offset.x, Offset.y);
 		if (ski_status_reg & 0x40) CPU_addIRQ();
 		ski_status_reg |= 0x80;
 		ski_status_reg &= ~0x02;
	}

}


void ADB_inputRightMouseDown() 
{
	if (adb_grab_mode == 1) 
   {
 		ski_modifier_reg |= 0x40;
	} 
   else if (adb_grab_mode == 2) 
   {
		ski_button1 = 1;
 		if (ski_status_reg & 0x80) return;	// Mouse reg still full
 		ski_xdelta = 0;
 		ski_ydelta = 0;
 		if (ski_status_reg & 0x40) CPU_addIRQ();
 		ski_status_reg |= 0x80;
 		ski_status_reg &= ~0x02;
	}
}

void ADB_inputRightMouseUp() 
{
 	if (adb_grab_mode == 1) 
   {
		ski_modifier_reg &= ~0x40;
	} 
   else 
   if (adb_grab_mode == 2) 
   {
		ski_button1 = 0;
 		if (ski_status_reg & 0x80) return;	// Mouse reg still full
 		ski_xdelta = 0;
 		ski_ydelta = 0;
 		if (ski_status_reg & 0x40) CPU_addIRQ();
 		ski_status_reg |= 0x80;
 		ski_status_reg &= ~0x02;
	}
}

void ADB_inputLeftMouseDown() {
	if (adb_grab_mode == 1) {
 		ski_modifier_reg |= 0x80;
	} else if (adb_grab_mode == 2) {
		ski_button0 = 1;
 		if (ski_status_reg & 0x80) return;	// Mouse reg still full
 		ski_xdelta = 0;
 		ski_ydelta = 0;
 		if (ski_status_reg & 0x40) CPU_addIRQ();
 		ski_status_reg |= 0x80;
 		ski_status_reg &= ~0x02;
	}
}

void ADB_inputLeftMouseUp() {
	if (adb_grab_mode == 1) {
		ski_modifier_reg &= ~0x80;
	} else if (adb_grab_mode == 2) {
		ski_button0 = 0;
 		if (ski_status_reg & 0x80) return;	// Mouse reg still full
 		ski_xdelta = 0;
 		ski_ydelta = 0;
 		if (ski_status_reg & 0x40) CPU_addIRQ();
 		ski_status_reg |= 0x80;
 		ski_status_reg &= ~0x02;
	}
}

