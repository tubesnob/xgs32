/*

   COPYRIGHT NOTIFICATION

   This information is copyrighted by
   Steven W. Mentzer

   You may not sell or assume ownership of this information 
   without my express written consent.

   This information can be freely redistributed, published, compiled, used or modified.

   Inquiries should be directed to   steve@staticmonkey.com

*/

#include "ddraw.h"
#include "vid-drv_utils.h"
#include "xgstypes.h"
#include "xgsmemory.h"
#include "xgsstate.h"
#include "video.h"
#include "vid-drv.h"
#include "hires.h"
#include "emul.h"
#include "vid-drv_pixel.h"

// Must run this proc to fill the RGB16 struct with the information needed to plot a pixel
// To call this, you must have rgb16 defined as a global (unless you want to modify this) variable
// RGB16 rgb16;
BOOL GetRGB16DEF(LPDIRECTDRAWSURFACE Surface)
{
   DDSURFACEDESC     ddsd;
   BYTE              shiftcount;
   
   // get a surface description
   ddsd.dwSize = sizeof(ddsd);
   ddsd.dwFlags = DDSD_PIXELFORMAT;
   if(Surface->GetSurfaceDesc(&ddsd) != DD_OK)
      return FALSE;

   // Fill in the masking values for extracting colors
   g_RGB16DEF.Mask.rgbRed = ddsd.ddpfPixelFormat.dwRBitMask;
   g_RGB16DEF.Mask.rgbGreen = ddsd.ddpfPixelFormat.dwGBitMask;
   g_RGB16DEF.Mask.rgbBlue = ddsd.ddpfPixelFormat.dwBBitMask;

   // get red
   shiftcount = 0;
   while(!(ddsd.ddpfPixelFormat.dwRBitMask & 1))
   {
      ddsd.ddpfPixelFormat.dwRBitMask >>= 1;
      shiftcount++;
   }
   g_RGB16DEF.depth.rgbRed = (BYTE)ddsd.ddpfPixelFormat.dwRBitMask;
   g_RGB16DEF.Position.rgbRed = shiftcount;
   g_RGB16DEF.Amount.rgbRed = (ddsd.ddpfPixelFormat.dwRBitMask == 0x1f) ? 3 : 2;

   // get green
   shiftcount = 0;
   while(!(ddsd.ddpfPixelFormat.dwGBitMask & 1))
   {
      ddsd.ddpfPixelFormat.dwGBitMask >>= 1;
      shiftcount++;
   }
   g_RGB16DEF.depth.rgbGreen = (BYTE)ddsd.ddpfPixelFormat.dwGBitMask;
   g_RGB16DEF.Position.rgbGreen = shiftcount;
   g_RGB16DEF.Amount.rgbGreen = (ddsd.ddpfPixelFormat.dwGBitMask == 0x1f) ? 3 : 2;
    
   // get Blue
   shiftcount = 0;
   while(!(ddsd.ddpfPixelFormat.dwBBitMask & 1))
   {
      ddsd.ddpfPixelFormat.dwBBitMask >>= 1;
      shiftcount++;
   }
   g_RGB16DEF.depth.rgbBlue = (BYTE)ddsd.ddpfPixelFormat.dwBBitMask;
   g_RGB16DEF.Position.rgbBlue = shiftcount;
   g_RGB16DEF.Amount.rgbBlue = (ddsd.ddpfPixelFormat.dwBBitMask == 0x1f) ? 3 : 2;
    
   return TRUE;
}


void WriteDDError(long errnum)
{
   SETFUNCTION("WriteDDError");
   switch (errnum)
   {
      case DDERR_INCOMPATIBLEPRIMARY:  
         WriteDebug("INCOMPATIBLEPRIMARY");
         break;
      case DDERR_INVALIDCAPS:  
         WriteDebug("INVALIDCAPS  ");
         break;
      case DDERR_INVALIDOBJECT:  
         WriteDebug("INVALIDOBJECT  ");
         break;
      case DDERR_INVALIDPARAMS:  
         WriteDebug("INVALIDPARAMS  ");
         break;
      case DDERR_INVALIDPIXELFORMAT:  
         WriteDebug("INVALIDPIXELFORMAT  ");
         break;
      case DDERR_NOALPHAHW:  
         WriteDebug("NOALPHAHW  ");
         break;
      case DDERR_NOCOOPERATIVELEVELSET:  
         WriteDebug("NOCOOPERATIVELEVELSET  ");
         break;
      case DDERR_NODIRECTDRAWHW:  
         WriteDebug("NODIRECTDRAWHW  ");
         break;
      case DDERR_NOEMULATION:  
         WriteDebug("NOEMULATION  ");
         break;
      case DDERR_NOEXCLUSIVEMODE:  
         WriteDebug("NOEXCLUSIVEMODE  ");
         break;
      case DDERR_NOFLIPHW:  
         WriteDebug("NOFLIPHW  ");
         break;
      case DDERR_NOMIPMAPHW:  
         WriteDebug("NOMIPMAPHW  ");
         break;
      case DDERR_NOOVERLAYHW:  
         WriteDebug("NOOVERLAYHW  ");
         break;
      case DDERR_NOZBUFFERHW:  
         WriteDebug("NOZBUFFERHW  ");
         break;
      case DDERR_OUTOFMEMORY:  
         WriteDebug("OUTOFMEMORY  ");
         break;
      case DDERR_OUTOFVIDEOMEMORY:  
         WriteDebug("OUTOFVIDEOMEMORY  ");
         break;
      case DDERR_PRIMARYSURFACEALREADYEXISTS:  
         WriteDebug("PRIMARYSURFACEALREADYEXISTS  ");
         break;
      case DDERR_UNSUPPORTEDMODE:  
         WriteDebug("UNSUPPORTEDMODE  ");
         break;
      case DDERR_SURFACEBUSY:  
         WriteDebug("SURFACEBUSY");
         break;
      case DDERR_SURFACELOST:  
         WriteDebug("SURFACELOST");
         break;
      case DDERR_WASSTILLDRAWING:  
         WriteDebug("WASSTILLDRAWING");
         break;
      case DDERR_EXCEPTION  :  
         WriteDebug("EXCEPTION");
         break;
      case DDERR_GENERIC  :  
         WriteDebug("GENERIC");
         break;
      case DDERR_INVALIDRECT  :  
         WriteDebug("INVALIDRECT");
         break;
      case DDERR_NOBLTHW  :  
         WriteDebug("NOBLTHW");
         break;
      case DDERR_UNSUPPORTED  :  
         WriteDebug("UNSUPPORTED");
         break;
      default:
         sprintf(emul_buffer,"Unknown DD Error 0x%xd",errnum);
         WriteDebug(emul_buffer);
   };
};

HRESULT DDCreateOffscreenSurface(IDirectDraw *pdd, long DX, long DY, LPDIRECTDRAWSURFACE *ppdds, DDSURFACEDESC *pddsd)
{
   HRESULT hr;
   ZeroMemory(pddsd, sizeof(*pddsd));
   pddsd->dwSize = sizeof(*pddsd);
   pddsd->dwFlags = DDSD_CAPS | DDSD_HEIGHT |DDSD_WIDTH;
   pddsd->ddsCaps.dwCaps = DDSCAPS_OFFSCREENPLAIN;
   pddsd->dwWidth = DX;
   pddsd->dwHeight = DY;

   hr = pdd->CreateSurface(pddsd, ppdds, NULL);

   return hr;
}


void VID_GenerateVidLines(int iType)
{
   // fill our vid_lines array with references to every line on the screen
   //
   // if iType=1: RAW screen coordinates at the start of every line
   // if iType=2: Screen coordinates offset by the size of the IIgs viewport
   //
   // THIS FUNCTION ASSUMES THAT THE g_DDBackBufferSurfaceDesc.lpSurface points to valid surface memory...

   byte  *screen;
   byte  *line;
   int   i;   
   DDSURFACEDESC        *pDDSurfaceDesc;
   LPDIRECTDRAWSURFACE  pDDSurface;

   VID_GetDrawingSurface(&pDDSurface,&pDDSurfaceDesc);

   if (iType==1)
      screen = ((byte*) pDDSurfaceDesc->lpSurface) + ((vid_out_y * pDDSurfaceDesc->lPitch) + (vid_out_x*BYTESPERPIXEL));
   else
      screen = (byte*) pDDSurfaceDesc->lpSurface;

   line           = screen;
   vid_lines[0]   = screen;

   for (i=1; i < g_lVidHeight; i++)
   {
      line += pDDSurfaceDesc->lPitch;
      vid_lines[i] = line;
   };
   
};

void VID_GetDrawingSurface(LPDIRECTDRAWSURFACE *pDDSurface, DDSURFACEDESC **pDDSurfaceDesc)
{
   if (g_lVidWindowed)
   {
      *pDDSurface     = g_pDDBackBuffer;
      *pDDSurfaceDesc = &g_DDBackBufferSurfaceDesc;
   }
   else
   {
      *pDDSurface     = g_pDDPrimarySurface;
      *pDDSurfaceDesc = &g_DDPrimarySurfaceDesc;
   };
}

