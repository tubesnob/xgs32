/*

   COPYRIGHT NOTIFICATION

   This information is copyrighted by
   Steven W. Mentzer

   You may not sell or assume ownership of this information 
   without my express written consent.

   This information can be freely redistributed, published, compiled, used or modified.

   Inquiries should be directed to   steve@staticmonkey.com

*/


#include "ddraw.h"
#include "atlbase.h"
#include "vid-drv_enum.h"
#include "xgsstate.h"
#include "xgstypes.h"
#include "emul.h"

typedef struct sModeType
               {
                  int   w;
                  int   h;
                  int   bpp;
                  int   refresh;
               } tModeType;
long        lNumDisplayModes;
tModeType   aDisplayModes[256];


HRESULT CALLBACK EnumDisplayModesCallback(LPDDSURFACEDESC pddsd, LPVOID Context)
{
    int ChkMode;

    for(ChkMode = 0;ChkMode < lNumDisplayModes;ChkMode++)
    {
        if(
            (aDisplayModes[ChkMode].w == (int)pddsd->dwWidth)&&
            (aDisplayModes[ChkMode].h == (int)pddsd->dwHeight)&&
            (aDisplayModes[ChkMode].bpp == (int)pddsd->ddpfPixelFormat.dwRGBBitCount)&& 
            (aDisplayModes[ChkMode].refresh == (int)pddsd->dwRefreshRate)
          )
            return DDENUMRET_OK;
    }

   if ((pddsd->dwWidth>639) && (pddsd->dwHeight>399) && (pddsd->ddpfPixelFormat.dwRGBBitCount > 7))
   {
    aDisplayModes[lNumDisplayModes].w       = pddsd->dwWidth;
    aDisplayModes[lNumDisplayModes].h       = pddsd->dwHeight;
    aDisplayModes[lNumDisplayModes].bpp     = pddsd->ddpfPixelFormat.dwRGBBitCount;
    aDisplayModes[lNumDisplayModes].refresh = pddsd->dwRefreshRate;
    lNumDisplayModes++;
   };

    return DDENUMRET_OK;
}

int VID_GetDisplayModes(VARIANT *pvarDisplayModes)
{
   SETFUNCTION("VID_GetDisplayModes");
   HRESULT        hr;
	LPDIRECTDRAW	pDirectDraw;
   LPDIRECTDRAW2  pDirectDraw2;
   SAFEARRAY   *psaDisplayModes;


	hr = DirectDrawCreate(NULL, &pDirectDraw, NULL);
	if (hr != DD_OK) 
   {
		sprintf(emul_buffer,"Failed To Create A DirectDraw Object", hr);
      WriteDebug(emul_buffer);
		return 1;
	}

	hr = pDirectDraw->QueryInterface(IID_IDirectDraw2,(void**) &pDirectDraw2);
   if (hr != DD_OK) 
   {
		WriteDebug("Failed to retrieve the DirectDraw2 interface");
		pDirectDraw->Release();
		return 1;
	}

   hr = pDirectDraw2->EnumDisplayModes(DDEDM_REFRESHRATES,NULL,NULL,EnumDisplayModesCallback);

	SAFEARRAYBOUND rgb[] = {{4, 0}, {lNumDisplayModes, 0}};
   psaDisplayModes=SafeArrayCreate(VT_VARIANT, 2, rgb);
   VARIANT vValue;
   vValue.vt = VT_I2;
   long lPos[2];
   for (int i=0; i < lNumDisplayModes; i++)
   {
         lPos[0] = 0;
         lPos[1] = i;
         vValue.intVal = aDisplayModes[i].h ;
         hr = SafeArrayPutElement(psaDisplayModes, lPos, &vValue);

         lPos[0] = 1;
         vValue.intVal = aDisplayModes[i].w;
         hr = SafeArrayPutElement(psaDisplayModes, lPos, &vValue);

         lPos[0] = 2;
         vValue.intVal = aDisplayModes[i].bpp;
         hr = SafeArrayPutElement(psaDisplayModes, lPos, &vValue);

         lPos[0] = 3;
         vValue.intVal = aDisplayModes[i].refresh;
         hr = SafeArrayPutElement(psaDisplayModes, lPos, &vValue);
   };

	   pvarDisplayModes->vt = VT_ARRAY | VT_VARIANT;
		pvarDisplayModes->parray = psaDisplayModes;
   return(1);
};
