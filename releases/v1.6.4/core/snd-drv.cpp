
#include "snd-drv.h"
#include "xgsstate.h"
#include "emul.h"


soundinfo   mybuffers[MAXSOUNDBUFFERS];
int         soundbuffer;
HWAVEOUT    phwo;
int         inited = 0;


void SND_PauseSound()
{
   SND_outputShutdown();
};
void SND_RestoreSoundAfterPause()
{
   SND_outputInit(SAMPLE_RATE);
};

int SND_outputInit(int rate)
{
   SETFUNCTION("SND_outputInit");
	PCMWAVEFORMAT soundformat;
	int i, j;

   WriteDebug("Initializing Windows Sound Driver");

	soundformat.wf.wFormatTag = WAVE_FORMAT_PCM;
	soundformat.wf.nChannels = 2;
	soundformat.wf.nSamplesPerSec = rate;
	soundformat.wf.nAvgBytesPerSec = rate * 4;
	soundformat.wf.nBlockAlign = 4;
	soundformat.wBitsPerSample = 16;

	if (waveOutOpen(&phwo, (UINT) WAVE_MAPPER, (LPWAVEFORMATEX) &soundformat, 
		(DWORD) g_hVidHWND, 0, CALLBACK_WINDOW) != MMSYSERR_NOERROR) 
   {
      DoErrorMessage("Unable To Initialize Wave Output Device");
		return 0;
	}

	for (i = 0; i < g_lSoundOutputBuffers; i++) 
   {
		mybuffers[i].hData = (char*)GlobalAlloc(GMEM_MOVEABLE | GMEM_SHARE, g_lSoundOutputBufferSize * 4);
		if (mybuffers[i].hData == NULL) 
      {
			for (j = 0; j < i; j++) GlobalFree(mybuffers[j].hData);
         DoErrorMessage("Unspecified Sound Driver Memory Error");
			return 0;
		}
	}

	for (i = 0; i < g_lSoundOutputBuffers; i++) 
   {
		if ((mybuffers[i].lpData = (char*)GlobalLock(mybuffers[i].hData)) == NULL) 
      {
			for (j = 0; j < i; j++) GlobalUnlock(mybuffers[j].hData);
			for (j = 0; j < g_lSoundOutputBuffers; j++) GlobalFree(mybuffers[j].hData);
         DoErrorMessage("Unspecified Sound Driver Memory Error");
			return 0;
		}
	}

	for (i = 0; i < g_lSoundOutputBuffers; i++) 
   {
		mybuffers[i].hWaveHdr = (char*)GlobalAlloc(GMEM_MOVEABLE | GMEM_SHARE,
			(DWORD) sizeof(WAVEHDR));
		if (!mybuffers[i].hWaveHdr) 
      {
			for (j = 0; j < i; j++) GlobalFree(mybuffers[j].hWaveHdr);
			for (j = 0; j < g_lSoundOutputBuffers; j++) GlobalUnlock(mybuffers[j].hData);
			for (j = 0; j < g_lSoundOutputBuffers; j++) GlobalFree(mybuffers[j].hData);
         DoErrorMessage("Unspecified Sound Driver Memory Error");
			return 0;
		}
	}

	for (i = 0; i < g_lSoundOutputBuffers; i++) 
   {
		mybuffers[i].lpWaveHdr = (LPWAVEHDR) (char*)GlobalLock(mybuffers[i].hWaveHdr);
		if (!mybuffers[i].lpWaveHdr) 
      {
			for (j = 0; j < i; j++) GlobalUnlock(mybuffers[j].hWaveHdr);
			for (j = 0; j < g_lSoundOutputBuffers; j++) GlobalFree(mybuffers[j].hWaveHdr);
			for (j = 0; j < g_lSoundOutputBuffers; j++) GlobalUnlock(mybuffers[j].hData);
			for (j = 0; j < g_lSoundOutputBuffers; j++) GlobalFree(mybuffers[j].hData);
		}
	}

	for (i = 0; i < g_lSoundOutputBuffers; i++) {
		mybuffers[i].lpWaveHdr->lpData = mybuffers[i].lpData;
		mybuffers[i].lpWaveHdr->dwBufferLength = g_lSoundOutputBufferSize * 4;
		mybuffers[i].lpWaveHdr->dwFlags = 0L;
		mybuffers[i].lpWaveHdr->dwLoops = 0L;
	}

	for (i = 0; i < g_lSoundOutputBuffers; i++)
		mybuffers[i].busy = FALSE;

	for (i = 0; i < g_lSoundOutputBuffers; i++)
		waveOutPrepareHeader(phwo, mybuffers[i].lpWaveHdr, sizeof(WAVEHDR));

	soundbuffer = 0;

	inited = 1;
	return rate;
}

void SND_outputShutdown(void)
{
   SETFUNCTION("SND_outputShutdown");
	int j;

	inited = 0;
   WriteDebug("Shutting Down Windows Sound Driver");

	if (waveOutReset(phwo) != MMSYSERR_NOERROR)
		WriteDebug("waveOutReset() failed.");

	for (j = 0; j < g_lSoundOutputBuffers; j++)
		waveOutUnprepareHeader(phwo, mybuffers[j].lpWaveHdr, sizeof(WAVEHDR));

	for (j = 0; j < g_lSoundOutputBuffers; j++) GlobalUnlock(mybuffers[j].hWaveHdr);
	for (j = 0; j < g_lSoundOutputBuffers; j++) GlobalFree(mybuffers[j].hWaveHdr);
	for (j = 0; j < g_lSoundOutputBuffers; j++) GlobalUnlock(mybuffers[j].hData);
	for (j = 0; j < g_lSoundOutputBuffers; j++) GlobalFree(mybuffers[j].hData);

	if (waveOutClose(phwo) != MMSYSERR_NOERROR)
		WriteDebug("waveOutClose() failed.");
}

size_t SND_outputWrite(snd_sample_struct *buffer, size_t len)
{
   SETFUNCTION("SND_outputWrite");
	UINT wResult;
	int i;

	if (!inited) return 0;
   if (len > g_lSoundOutputBufferSize)
   {
      len = g_lSoundOutputBufferSize;
      WriteDebug("Output Buffer Exceeds Configured Sound Buffer Length. Sound Is Clipping");
   };  

	soundbuffer = -1;
	i = 0;
	while ((i < g_lSoundOutputBuffers) && (soundbuffer == -1))
		if (mybuffers[i++].busy == FALSE) soundbuffer = i-1;

	if (soundbuffer == -1) 
   {
      WriteDebug("Buffer Count Underrun. Configure the emulator to provide more available sound buffers!");
      return 0;
   };

   mybuffers[soundbuffer].lpWaveHdr->dwBufferLength=len*4;
	mybuffers[soundbuffer].lpWaveHdr->dwFlags = WHDR_PREPARED;
	mybuffers[soundbuffer].busy = TRUE;

   //	memcpy(mybuffers[soundbuffer].lpData, buffer, g_lSoundOutputBufferSize);
	for (i = 0; i < len; i++) 
   {
		((WORD *) (mybuffers[soundbuffer].lpData))[i*2] = (WORD) (buffer[i].left >> 2);
		((WORD *) (mybuffers[soundbuffer].lpData))[(i*2)+1] = (WORD) (buffer[i].right >> 2);
	}
	wResult = waveOutWrite(phwo, mybuffers[soundbuffer].lpWaveHdr, sizeof(WAVEHDR));
	if (wResult != 0) 
   {
      WriteDebug("Unable to output wave data!");
		return 0;
   };

	return len;
}
// -P-
