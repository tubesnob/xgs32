#ifndef __SOUND__
#define __SOUND__

   #include "xgstypes.h"

   int  SND_init(void);
   void SND_update(void);
   void SND_reset(void);
   void SND_shutdown(void);
   byte SND_clickSpeaker(byte);
   byte SND_readSoundCtl(byte);
   byte SND_readSoundData(byte);
   byte SND_readSoundAddrL(byte);
   byte SND_readSoundAddrH(byte);
   byte SND_writeSoundCtl(byte);
   byte SND_writeSoundData(byte);
   byte SND_writeSoundAddrL(byte);
   byte SND_writeSoundAddrH(byte);
   void SND_pushIRQ(int);
   int  SND_pullIRQ(void);
   void SND_enableOscillators(void);
   void SND_updateOscillator(int);
   void SND_scanOscillators(snd_sample_struct *);
   void SND_updateClassicSound(snd_sample_struct *);


#endif
