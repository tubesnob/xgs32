
#ifndef __XGSDISPATCH__
#define  __XGS_DISPATCH__

   #include "xgstypes.h"

   extern   void (*cpu_opcode_table[1300]);
   extern   duala		atmp,opaddr;
   extern   dualw		wtmp,otmp,operand;
   extern   int		a1,a2,a3,a4,o1,o2,o3,o4;

   extern   long     g_lDispatchDebug;
   extern   long     g_lDebugRun;
#endif