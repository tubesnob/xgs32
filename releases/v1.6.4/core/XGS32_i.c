/* this file contains the actual definitions of */
/* the IIDs and CLSIDs */

/* link this file in with the server and any clients */


/* File created by MIDL compiler version 5.01.0164 */
/* at Sat Jul 21 12:52:26 2001
 */
/* Compiler settings for E:\source\xgs32\core\XGS32.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )
#ifdef __cplusplus
extern "C"{
#endif 


#ifndef __IID_DEFINED__
#define __IID_DEFINED__

typedef struct _IID
{
    unsigned long x;
    unsigned short s1;
    unsigned short s2;
    unsigned char  c[8];
} IID;

#endif // __IID_DEFINED__

#ifndef CLSID_DEFINED
#define CLSID_DEFINED
typedef IID CLSID;
#endif // CLSID_DEFINED

const IID IID_IXGS32Obj = {0xF15833FE,0x914D,0x11D3,{0xA0,0x8D,0x00,0x90,0x27,0x8C,0x0E,0xC7}};


const IID LIBID_XGS32Lib = {0xB84945E1,0x8E29,0x11D3,{0xA0,0x8B,0x00,0x90,0x27,0x8C,0x0E,0xC7}};


const IID DIID__IXGS32ObjEvents = {0xF1583400,0x914D,0x11D3,{0xA0,0x8D,0x00,0x90,0x27,0x8C,0x0E,0xC7}};


const CLSID CLSID_XGS32Obj = {0xF15833FF,0x914D,0x11D3,{0xA0,0x8D,0x00,0x90,0x27,0x8C,0x0E,0xC7}};


#ifdef __cplusplus
}
#endif

