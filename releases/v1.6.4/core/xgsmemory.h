#ifndef __XGSMEMORY__
#define __XGSMEMORY__

   #include "xgstypes.h"
   #include "rw.h"

   extern   mem_pagestruct	   mem_pages[65536];
   extern   int	mem_80store;
   extern   int	mem_diagtype;
   extern   byte	*slow_memory;
   extern   word32	mem_change_masks[256];
   extern   word32	mem_slowram_changed[512];


   int   MEM_init(void);
   int	MEM_init(void);
   void	MEM_update(void);
   void	MEM_reset(void);
   void	MEM_shutdown(void);
   void	MEM_rebuildMainMem(void);
   void	MEM_rebuildAltZpMem(void);
   void	MEM_rebuildLangCardMem(void);
   void	MEM_rebuildShadowMem(void);
   void	MEM_buildLanguageCard(int, int,byte *);
   byte	MEM_ioUnimp(byte);
   byte	MEM_clear80store(byte val);
   byte	MEM_set80store(byte val);
   byte	MEM_get80store(byte val);
   byte	MEM_clearAuxRd(byte val);
   byte	MEM_setAuxRd(byte val);
   byte	MEM_getAuxRd(byte val);
   byte	MEM_clearAuxWrt(byte val);
   byte	MEM_setAuxWrt(byte val);
   byte	MEM_getAuxWrt(byte val);
   byte	MEM_clearIntCXROM(byte val);
   byte	MEM_setIntCXROM(byte val);
   byte	MEM_getIntCXROM(byte val);
   byte	MEM_clearAltZP(byte val);
   byte	MEM_setAltZP(byte val);
   byte	MEM_getAltZP(byte val);
   byte	MEM_clearSlotC3ROM(byte val);
   byte	MEM_setSlotC3ROM(byte val);
   byte	MEM_getSlotC3ROM(byte val);
   byte	MEM_getLCbank(byte val);
   byte	MEM_getLCread(byte val);
   byte	MEM_getSlotReg(byte val);
   byte	MEM_setSlotReg(byte val);
   byte	MEM_getShadowReg(byte val);
   byte	MEM_setShadowReg(byte val);
   byte	MEM_getCYAReg(byte val);
   byte	MEM_setCYAReg(byte val);
   byte	MEM_getIntEnReg(byte val);
   byte	MEM_setIntEnReg(byte val);
   byte	MEM_getDiagType(byte val);
   byte	MEM_setVBLClear(byte val);
   byte	MEM_getStateReg(byte val);
   byte	MEM_setStateReg(byte val);
   byte	MEM_setLCx80(byte val);
   byte	MEM_setLCx81(byte val);
   byte	MEM_setLCx82(byte val);
   byte	MEM_setLCx83(byte val);
   byte	MEM_setLCx88(byte val);
   byte	MEM_setLCx89(byte val);
   byte	MEM_setLCx8A(byte val);
   byte	MEM_setLCx8B(byte val);
   byte	MEM_getC071(byte val);
   byte	MEM_getC072(byte val);
   byte	MEM_getC073(byte val);
   byte	MEM_getC074(byte val);
   byte	MEM_getC075(byte val);
   byte	MEM_getC076(byte val);
   byte	MEM_getC077(byte val);
   byte	MEM_getC078(byte val);
   byte	MEM_getC079(byte val);
   byte	MEM_getC07A(byte val);
   byte	MEM_getC07B(byte val);
   byte	MEM_getC07C(byte val);
   byte	MEM_getC07D(byte val);
   byte	MEM_getC07E(byte val);
   byte	MEM_getC07F(byte val);

   //byte MEM_readMem(word32 addr);
   //void MEM_writeMem(word32 addr, byte val);


   __forceinline void MEM_writeMem2(word32 addr, byte val)
   {
	   int	page;
	   byte	offset;
	   word16	flags;
	   
	   page = (addr >> 8);
	   offset = (byte) addr;
	   flags = mem_pages[page].writeFlags;
	   if (flags & MEM_FLAG_INVALID) return;
	   if (flags & MEM_FLAG_IO) 
      {
         //sprintf(emul_buffer,"Dispatching Write I/O %d\n",offset&0xFF);
         //WriteDebug(emul_buffer);
		   (*mem_io_write_dispatch[offset])(val);
		   return;
	   }
	   if (flags & MEM_FLAG_SPECIAL) {
		   if (*(mem_pages[page].writePtr + offset) != val) {
			   mem_slowram_changed[page & 0x1FF] |= mem_change_masks[offset];
			   *(mem_pages[page].writePtr + offset) = val;
		   }
		   return;
	   }
	   *(mem_pages[page].writePtr + offset) = val;
	   if (flags & MEM_FLAG_SHADOW_E0) {
		   page = (page & 0xFF) + 0xE000;
		   if (*(mem_pages[page].writePtr + offset) != val) {
			   mem_slowram_changed[page & 0x1FF] |= mem_change_masks[offset];
			   *(mem_pages[page].writePtr + offset) = val;
		   }
	   } else if (flags & MEM_FLAG_SHADOW_E1) {
		   page = (page & 0xFF) + 0xE100;
		   if (*(mem_pages[page].writePtr + offset) != val) {
			   mem_slowram_changed[page & 0x1FF] |= mem_change_masks[offset];
			   *(mem_pages[page].writePtr + offset) = val;
		   }
	   }
   }


   __forceinline MEM_writeMem(word32 addr, byte val)
   {
	   word16 page = (word16) (addr >> 8);
      g_lIOAddress = addr;
	   if ( mem_pages[page].writeFlags)
		   MEM_writeMem2(addr,val);
	   else
		   *( mem_pages[page].writePtr + (addr & 0xFF)) = val;
   };


   __forceinline byte MEM_readMem(word32 addr)
   {
	   word16 page = (word16) (addr >> 8);
      g_lIOAddress = addr;
	   if ( mem_pages[page].readFlags & MEM_FLAG_IO)
      {
		    return (*mem_io_read_dispatch[addr & 0xFF])(0);
      }
	   else
         return *(mem_pages[page].readPtr + (addr & 0xFF));
   };



#endif /* _GSMEMORY_H_ */
