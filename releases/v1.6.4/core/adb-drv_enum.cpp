/*

   COPYRIGHT NOTIFICATION

   This information is copyrighted by
   Steven W. Mentzer

   You may not sell or assume ownership of this information 
   without my express written consent.

   This information can be freely redistributed, published, compiled, used or modified.

   Inquiries should be directed to   steve@staticmonkey.com

*/


#include "atlbase.h"
#define  DIRECTINPUT_VERSION 0x0300 
#include "dinput.h"
#include "ddraw.h"
#include "adb-drv_enum.h"
#include "xgsstate.h"

long lNumInputDevices=0;
typedef struct sInputDevice
					{
                  BSTR  bstrDeviceType;
                  BSTR  bstrDeviceSubType;
                  BSTR  bstrDeviceInstanceGUID;
                  BSTR  bstrDeviceInstanceName;
                  BSTR  bstrDeviceProductName;
					} tInputDevice;
tInputDevice aInputDevices[256];

//BOOL _stdcall ADB_EnumJoySticks(LPDIDEVICEINSTANCE lpddi, LPVOID pvRef);
void ADB_GetDeviceType(DWORD dwDeviceType, BSTR *bstrType, BSTR *bstrSubType)
{
   BSTR  bstrTemp;
   unsigned int iSubType;
   unsigned int iType;

   *bstrType = NULL;
   *bstrSubType = NULL;
   iType = (dwDeviceType&0x00FF);
   iSubType = ((dwDeviceType&0xFF00)>>8);

   if (iType==DIDEVTYPE_MOUSE)
   {
      *bstrType = SysAllocString(L"Mouse");

      switch (iSubType)
      {
         case DIDEVTYPEMOUSE_UNKNOWN:
            bstrTemp = L"Unknown";               
            break;
         case DIDEVTYPEMOUSE_TRADITIONAL:
            bstrTemp = L"Traditional";               
            break;
         case DIDEVTYPEMOUSE_FINGERSTICK:  
            bstrTemp = L"Fingerstick";               
            break;
         case DIDEVTYPEMOUSE_TOUCHPAD:  
            bstrTemp = L"Touchpad";     
            break;          
         case DIDEVTYPEMOUSE_TRACKBALL:  
            bstrTemp = L"Trackball";     
            break;          
         default:
            bstrTemp = L"(error returning subtype)";
            break;
      };
   }
   else
   if (iType==DIDEVTYPE_KEYBOARD)
   {
      *bstrType = SysAllocString(L"Keyboard");
      switch(iSubType)
      {

         case DIDEVTYPEKEYBOARD_UNKNOWN:     bstrTemp=L"Unknown"; break;
         case DIDEVTYPEKEYBOARD_PCXT:        bstrTemp=L"PC/XT"; break;
         case DIDEVTYPEKEYBOARD_OLIVETTI:    bstrTemp=L"OLIVETTI"; break;
         case DIDEVTYPEKEYBOARD_PCAT:        bstrTemp=L"PC/AT"; break;
         case DIDEVTYPEKEYBOARD_PCENH:       bstrTemp=L"PC/AT Enhanced"; break;  
         case DIDEVTYPEKEYBOARD_NOKIA1050:   bstrTemp=L"NOKIA 1050"; break;
         case DIDEVTYPEKEYBOARD_NOKIA9140:   bstrTemp=L"NOKIA 9140"; break; 
         case DIDEVTYPEKEYBOARD_NEC98:       bstrTemp=L"NEC 98"; break;
         case DIDEVTYPEKEYBOARD_NEC98LAPTOP: bstrTemp=L"NEC 98 Laptop"; break;
         case DIDEVTYPEKEYBOARD_NEC98106:    bstrTemp=L"NEC 98 106key"; break;
         case DIDEVTYPEKEYBOARD_JAPAN106:    bstrTemp=L"Japan 106"; break;
         case DIDEVTYPEKEYBOARD_JAPANAX:     bstrTemp=L"Japan AX"; break;  
         case DIDEVTYPEKEYBOARD_J3100:       bstrTemp=L"J3100"; break;  
         default: bstrTemp = L"(error returning subtype)"; break;
      };
   }
   else
   if (iType==DIDEVTYPE_JOYSTICK)
   {
      *bstrType = SysAllocString(L"Joystick");
      switch(iSubType)
      {
         case DIDEVTYPEJOYSTICK_UNKNOWN:        bstrTemp=L"Unknown"; break; 
         case DIDEVTYPEJOYSTICK_TRADITIONAL:    bstrTemp=L"Traditional"; break;
         case DIDEVTYPEJOYSTICK_FLIGHTSTICK:    bstrTemp=L"Flightstick"; break; 
         case DIDEVTYPEJOYSTICK_GAMEPAD:        bstrTemp=L"Gamepad"; break; 
         case DIDEVTYPEJOYSTICK_RUDDER:         bstrTemp=L"Rudder"; break; 
         case DIDEVTYPEJOYSTICK_WHEEL:          bstrTemp=L"Wheel"; break; 
         case DIDEVTYPEJOYSTICK_HEADTRACKER:    bstrTemp=L"Headtracker"; break; 
         default: bstrTemp = L"(error returning subtype)";
      };

   }
   else
   if (iType==DIDEVTYPE_DEVICE)
   {
      *bstrType = SysAllocString(L"Unspecified/Generic Input Device");
      bstrTemp = L"N/A";
   }

   *bstrSubType = SysAllocString(bstrTemp);

};

BOOL _stdcall ADB_EnumJoySticks(LPDIDEVICEINSTANCE lpddi, LPVOID pvRef)
{
   char           *pszBuffer;
   tInputDevice   *Device;
   Device=&(aInputDevices[lNumInputDevices]);
   UuidToString(&lpddi->guidInstance,(unsigned char **)&pszBuffer);
   Device->bstrDeviceInstanceGUID = SysAllocString(CComBSTR(pszBuffer));
   ADB_GetDeviceType(lpddi->dwDevType,&(Device->bstrDeviceType),&(Device->bstrDeviceSubType));
   Device->bstrDeviceInstanceName = SysAllocString(CComBSTR(lpddi->tszInstanceName));
   Device->bstrDeviceProductName  = SysAllocString(CComBSTR(lpddi->tszProductName));
	lNumInputDevices++;
   return DDENUMRET_OK;
};

int ADB_GetAvailableJoySticks(VARIANT *pvarAvailableJoySticks)
{
   SETFUNCTION("ADB_GetAvailableJoySticks");
	HRESULT hr;
   SAFEARRAY   *psaAvailableJoysticks;
   LPDIRECTINPUT         pDirectInput;
   hr = DirectInputCreate(g_hDLLInstance,DIRECTINPUT_VERSION,&pDirectInput,NULL);
   if (SUCCEEDED(hr))
   {

      lNumInputDevices=0;
		pDirectInput->EnumDevices(NULL,(int (__stdcall *)(const struct DIDEVICEINSTANCEA *,void *))ADB_EnumJoySticks,NULL,DIEDFL_ALLDEVICES);
		pDirectInput->Release();
		pDirectInput=NULL;
      
		SAFEARRAYBOUND rgb[] = {{5, 0}, {lNumInputDevices, 0}};
		psaAvailableJoysticks=SafeArrayCreate(VT_VARIANT, 2, rgb);
		VARIANT vValue;
		vValue.vt = VT_BSTR;
		long lPos[2];
		for (int i=0; i < lNumInputDevices; i++)
		{
				lPos[0] = 0;
				lPos[1] = i;
				vValue.bstrVal = aInputDevices[i].bstrDeviceInstanceGUID;
				hr = SafeArrayPutElement(psaAvailableJoysticks, lPos, &vValue);

				lPos[0] = 1;
				vValue.bstrVal = aInputDevices[i].bstrDeviceInstanceName;
				hr = SafeArrayPutElement(psaAvailableJoysticks, lPos, &vValue);

				lPos[0] = 2;
				vValue.bstrVal = aInputDevices[i].bstrDeviceProductName;
				hr = SafeArrayPutElement(psaAvailableJoysticks, lPos, &vValue);

				lPos[0] = 3;
				vValue.bstrVal = aInputDevices[i].bstrDeviceType;
				hr = SafeArrayPutElement(psaAvailableJoysticks, lPos, &vValue);

				lPos[0] = 4;
				vValue.bstrVal = aInputDevices[i].bstrDeviceSubType;
				hr = SafeArrayPutElement(psaAvailableJoysticks, lPos, &vValue);

		};
		pvarAvailableJoySticks->vt = VT_ARRAY | VT_VARIANT;
		pvarAvailableJoySticks->parray = psaAvailableJoysticks;
   
	}
	else
	{
		WriteDebug("Failed To Create DirectInput ROOT object..\n");
	};		

	return(0);
};
