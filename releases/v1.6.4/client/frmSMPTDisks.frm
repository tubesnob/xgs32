VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "comdlg32.ocx"
Begin VB.Form frmSMPTDisks 
   AutoRedraw      =   -1  'True
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   6540
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6225
   LinkTopic       =   "Form1"
   ScaleHeight     =   436
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   415
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox edtSMPT 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   7
      Left            =   1320
      TabIndex        =   23
      Text            =   "Text1"
      Top             =   3720
      Width           =   3135
   End
   Begin VB.TextBox edtSMPT 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   6
      Left            =   1320
      TabIndex        =   22
      Text            =   "Text1"
      Top             =   3240
      Width           =   3135
   End
   Begin VB.TextBox edtSMPT 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   5
      Left            =   1320
      TabIndex        =   21
      Text            =   "Text1"
      Top             =   2760
      Width           =   3135
   End
   Begin VB.TextBox edtSMPT 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   4
      Left            =   1320
      TabIndex        =   20
      Text            =   "Text1"
      Top             =   2280
      Width           =   3135
   End
   Begin VB.TextBox edtSMPT 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   1320
      TabIndex        =   19
      Text            =   "Text1"
      Top             =   1800
      Width           =   3135
   End
   Begin VB.TextBox edtSMPT 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   1320
      TabIndex        =   18
      Text            =   "Text1"
      Top             =   1320
      Width           =   3135
   End
   Begin VB.TextBox edtSMPT 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   1320
      TabIndex        =   17
      Text            =   "Text1"
      Top             =   840
      Width           =   3135
   End
   Begin VB.TextBox edtSMPT 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   1320
      TabIndex        =   8
      Text            =   "Text1"
      Top             =   360
      Width           =   3135
   End
   Begin VB.CommandButton ChangeSMPT 
      Height          =   375
      Index           =   0
      Left            =   4680
      Picture         =   "frmSMPTDisks.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   7
      Top             =   360
      Width           =   375
   End
   Begin VB.CommandButton ChangeSMPT 
      Height          =   375
      Index           =   7
      Left            =   4680
      Picture         =   "frmSMPTDisks.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   6
      Top             =   3720
      Width           =   375
   End
   Begin VB.CommandButton ChangeSMPT 
      Height          =   375
      Index           =   6
      Left            =   4680
      Picture         =   "frmSMPTDisks.frx":0A64
      Style           =   1  'Graphical
      TabIndex        =   5
      Top             =   3240
      Width           =   375
   End
   Begin VB.CommandButton ChangeSMPT 
      Height          =   375
      Index           =   5
      Left            =   4680
      Picture         =   "frmSMPTDisks.frx":0F96
      Style           =   1  'Graphical
      TabIndex        =   4
      Top             =   2760
      Width           =   375
   End
   Begin VB.CommandButton ChangeSMPT 
      Height          =   375
      Index           =   4
      Left            =   4680
      Picture         =   "frmSMPTDisks.frx":14C8
      Style           =   1  'Graphical
      TabIndex        =   3
      Top             =   2280
      Width           =   375
   End
   Begin VB.CommandButton ChangeSMPT 
      Height          =   375
      Index           =   3
      Left            =   4680
      Picture         =   "frmSMPTDisks.frx":19FA
      Style           =   1  'Graphical
      TabIndex        =   2
      Top             =   1800
      Width           =   375
   End
   Begin VB.CommandButton ChangeSMPT 
      Height          =   375
      Index           =   2
      Left            =   4680
      Picture         =   "frmSMPTDisks.frx":1F2C
      Style           =   1  'Graphical
      TabIndex        =   1
      Top             =   1320
      Width           =   375
   End
   Begin VB.CommandButton ChangeSMPT 
      Height          =   375
      Index           =   1
      Left            =   4680
      Picture         =   "frmSMPTDisks.frx":245E
      Style           =   1  'Graphical
      TabIndex        =   0
      Top             =   840
      Width           =   375
   End
   Begin MSComDlg.CommonDialog dlgFileOpen 
      Left            =   240
      Top             =   -240
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label8 
      Caption         =   "SMPT7"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   16
      Top             =   3720
      Width           =   975
   End
   Begin VB.Label Label7 
      Caption         =   "SMPT6"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   15
      Top             =   3240
      Width           =   975
   End
   Begin VB.Label Label6 
      Caption         =   "SMPT5"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   14
      Top             =   2760
      Width           =   975
   End
   Begin VB.Label Label5 
      Caption         =   "SMPT4"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   13
      Top             =   2280
      Width           =   975
   End
   Begin VB.Label Label4 
      Caption         =   "SMPT3"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   12
      Top             =   1800
      Width           =   975
   End
   Begin VB.Label Label3 
      Caption         =   "SMPT2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   11
      Top             =   1320
      Width           =   975
   End
   Begin VB.Label Label2 
      Caption         =   "SMPT1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   10
      Top             =   840
      Width           =   975
   End
   Begin VB.Label Label1 
      Caption         =   "SMPT0"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   9
      Top             =   360
      Width           =   975
   End
End
Attribute VB_Name = "frmSMPTDisks"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub CloseForm()
    Unload Me
End Sub

Public Sub SaveSettings()
    SaveRegDefaults
End Sub

Private Sub edtSMPT_LostFocus(Index As Integer)
    AssignSmartPorts
End Sub

Private Sub Form_Load()
    ResizeChildWindow Me.hwnd, frmMain.picMain.hwnd
    SetWindowTitle "SMPT (Smartport) Disk Image Assignments"
    DisplaySmartports
End Sub

Private Sub ChangeSMPT_Click(Index As Integer)
    On Error GoTo UserCancel
    dlgFileOpen.CancelError = True
    dlgFileOpen.Filter = "All files (*.*)|*.*|2MG Images (*.2mg)|*.2mg|DSK Images(*.dsk)|*.dsk|Dos Order Image (*.do)|*.do|ProDos Order Images (*.po)|*.po|Old-Style XGS Images (*.xgs)|*.xgs"
    dlgFileOpen.FilterIndex = 2
    dlgFileOpen.DialogTitle = "Select SMPT" & Trim(Str(Index)) & " Disk Image."
    dlgFileOpen.DefaultExt = "*.2mg"
    dlgFileOpen.ShowOpen
    edtSMPT(Index) = dlgFileOpen.FileName
    AssignSmartPorts
    Exit Sub
UserCancel:
    Exit Sub
End Sub

Private Sub AssignSmartPorts()
    Dim i As Integer
    For i = 0 To 7
        If (g_strSmartPorts(i) <> edtSMPT(i)) And ((g_objXGSServer.RunState = 2) Or (g_objXGSServer.RunState = 0)) Then
            If (g_strSmartPorts(i) <> edtSMPT(i)) Then
                g_objXGSServer.SmartPort(i) = edtSMPT(i)
            End If
        End If
        g_strSmartPorts(i) = edtSMPT(i)
    Next
End Sub

Private Sub DisplaySmartports()
    Dim i As Integer
    For i = 0 To 7
        edtSMPT(i) = g_strSmartPorts(i)
    Next
End Sub

Private Sub Form_Unload(Cancel As Integer)
    SaveRegDefaults
End Sub
