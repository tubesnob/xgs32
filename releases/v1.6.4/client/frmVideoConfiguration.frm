VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "comctl32.ocx"
Begin VB.Form frmVideoConfiguration 
   AutoRedraw      =   -1  'True
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   6330
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7635
   LinkTopic       =   "Form1"
   ScaleHeight     =   422
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   509
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkLockVideoUpdates 
      Caption         =   "Lock Video Updates"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   360
      TabIndex        =   5
      Top             =   3720
      Width           =   3975
   End
   Begin VB.CheckBox chkDisplayWindowInClientArea 
      Caption         =   "Display Video In XGS/32 Client Area"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   4
      Top             =   2640
      Width           =   4215
   End
   Begin VB.ListBox lstResolutions 
      BeginProperty Font 
         Name            =   "Lucida Console"
         Size            =   9.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   1035
      Left            =   360
      TabIndex        =   1
      Top             =   600
      Width           =   6855
   End
   Begin VB.CheckBox chkVidWindowed 
      Caption         =   "Run In A Window On The Desktop"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   360
      TabIndex        =   0
      Top             =   1920
      Width           =   4455
   End
   Begin ComctlLib.Slider slTargetVideoRefreshRate 
      Height          =   375
      Left            =   3240
      TabIndex        =   6
      Top             =   4560
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   661
      _Version        =   327682
      Min             =   1
      Max             =   60
      SelStart        =   1
      TickStyle       =   3
      Value           =   1
   End
   Begin VB.Label Label5 
      Caption         =   $"frmVideoConfiguration.frx":0000
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   720
      TabIndex        =   10
      Top             =   5040
      Width           =   4575
   End
   Begin VB.Label Label3 
      Caption         =   $"frmVideoConfiguration.frx":00B9
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   720
      TabIndex        =   9
      Top             =   3960
      Width           =   4815
   End
   Begin VB.Label Label2 
      Caption         =   $"frmVideoConfiguration.frx":0153
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   720
      TabIndex        =   8
      Top             =   3000
      Width           =   4815
   End
   Begin VB.Label labTargetVideoRefreshRate 
      Caption         =   "Target Video Refresh Rate"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   7
      Top             =   4560
      Width           =   2655
   End
   Begin VB.Label Label4 
      Caption         =   $"frmVideoConfiguration.frx":0200
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   720
      TabIndex        =   3
      Top             =   2160
      Width           =   4815
   End
   Begin VB.Label Label1 
      Caption         =   "Selected Video Mode / Window Size"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   360
      TabIndex        =   2
      Top             =   360
      Width           =   3855
   End
End
Attribute VB_Name = "frmVideoConfiguration"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Dim varModes As Variant

Public Sub CloseForm()
    Unload Me
End Sub

Public Sub SaveSettings()
    g_dTargetVideoRefreshRate = CDbl(slTargetVideoRefreshRate.Value)
    g_lLockVideoUpdates = chkLockVideoUpdates.Value
    Dim iModeNumber As Integer
    iModeNumber = lstResolutions.ListIndex
    If iModeNumber <> -1 Then
        g_lVideoHeight = varModes(0, iModeNumber)
        g_lVideoWidth = varModes(1, iModeNumber)
        g_lVideoBitDepth = varModes(2, iModeNumber)
        g_lVideoRefreshRate = varModes(3, iModeNumber)
        g_lVidWindowed = chkVidWindowed
        g_lDisplayVideoInClient = chkDisplayWindowInClientArea.Value
    End If
    SaveRegDefaults
End Sub

Private Function GetModeString(ModeNumber) As String
    Dim h As Long
    Dim w As Long
    Dim bpp As Long
    Dim Refresh As Long
    h = varModes(0, ModeNumber)
    w = varModes(1, ModeNumber)
    bpp = varModes(2, ModeNumber)
    Refresh = varModes(3, ModeNumber)
    GetModeString = BuildModeString(w, h, bpp, Refresh)
End Function

Private Function BuildModeString(w, h, bpp, Refresh As Long) As String
    BuildModeString = w & "x" & h & "x" & bpp & " @ " & Refresh & "Hz"
End Function

Private Function ResolveModeNumber(ModeString As String) As Integer
    Dim rv As Integer
    Dim i As Integer
    rv = -1
    For i = 0 To UBound(varModes, 2)
        If GetModeString(i) = ModeString Then
            rv = i
        End If
    Next
    ResolveModeNumber = rv
End Function

Private Sub chkDisplayWindowInClientArea_Click()
    RefreshForm
End Sub

Private Sub chkLockVideoUpdates_Click()
    RefreshForm
End Sub


Private Sub chkVidWindowed_Click()
    RefreshForm
End Sub

Private Sub Form_Load()
    Dim i As Integer
    Dim strModeString As String
    
    ResizeChildWindow Me.hwnd, frmMain.picMain.hwnd
    SetWindowTitle "Video Configuration"
    
    ' Get all of our available video modes...
    varModes = g_objXGSServer.ValidDisplayModes
    lstResolutions.Clear
    For i = 0 To UBound(varModes, 2)
        lstResolutions.AddItem GetModeString(i)
    Next
    strModeString = BuildModeString(g_lVideoWidth, g_lVideoHeight, g_lVideoBitDepth, g_lVideoRefreshRate)
    i = ResolveModeNumber(strModeString)
    If (i <> -1) Then
     lstResolutions.ListIndex = i
    Else
     lstResolutions.ListIndex = 1
    End If
    slTargetVideoRefreshRate.Value = CInt(g_dTargetVideoRefreshRate)
    chkVidWindowed = g_lVidWindowed
    chkLockVideoUpdates.Value = g_lLockVideoUpdates
    chkDisplayWindowInClientArea.Value = g_lDisplayVideoInClient
    RefreshForm
End Sub

Public Sub RefreshForm()
    labTargetVideoRefreshRate = "Target Video Refresh Rate (" & slTargetVideoRefreshRate.Value & "/sec)"
    
    If chkVidWindowed.Value Then
        chkDisplayWindowInClientArea.Enabled = True
    Else
        chkDisplayWindowInClientArea.Enabled = False
        chkDisplayWindowInClientArea.Value = False
    End If
    
    If chkLockVideoUpdates.Value Then
        slTargetVideoRefreshRate.Enabled = False
        labTargetVideoRefreshRate.Enabled = False
    Else
        slTargetVideoRefreshRate.Enabled = True
        labTargetVideoRefreshRate.Enabled = True
    End If
End Sub


Private Sub slTargetVideoRefreshRate_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button Then
        RefreshForm
    End If
End Sub


