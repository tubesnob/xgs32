VERSION 5.00
Begin VB.Form frmAbout 
   BorderStyle     =   0  'None
   Caption         =   "About XGS/32"
   ClientHeight    =   4920
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5580
   LinkTopic       =   "Form1"
   MaxButton       =   0   'False
   MinButton       =   0   'False
   ScaleHeight     =   328
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   372
   ShowInTaskbar   =   0   'False
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      AutoSize        =   -1  'True
      BackColor       =   &H80000000&
      Caption         =   "XGS/32 v1.6"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   18
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      ForeColor       =   &H80000002&
      Height          =   435
      Left            =   1560
      TabIndex        =   7
      Top             =   360
      Width           =   2205
   End
   Begin VB.Label Label2 
      Alignment       =   2  'Center
      Caption         =   "Written By Steven W. Mentzer"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   6
      Top             =   840
      Width           =   5175
   End
   Begin VB.Label Label3 
      Alignment       =   2  'Center
      Caption         =   "XGS/32 is a Win32 Native port of the original XGS core written by Josh Thomson. "
      Height          =   495
      Left            =   840
      TabIndex        =   5
      Top             =   2280
      Width           =   3855
   End
   Begin VB.Label Label4 
      Alignment       =   2  'Center
      Caption         =   $"frmAbout.frx":0000
      Height          =   735
      Left            =   840
      TabIndex        =   4
      Top             =   2880
      Width           =   3855
   End
   Begin VB.Label Label5 
      Alignment       =   2  'Center
      Caption         =   "The client you are using was written in Visual Basic v6.0"
      Height          =   255
      Left            =   840
      TabIndex        =   3
      Top             =   3720
      Width           =   3855
   End
   Begin VB.Label Label7 
      Alignment       =   2  'Center
      Caption         =   "email: smentzer@pacbell.net"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   8.25
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   120
      TabIndex        =   2
      Top             =   1080
      Width           =   5175
   End
   Begin VB.Label labClientBuild 
      Alignment       =   2  'Center
      Caption         =   "Client Build"
      Height          =   255
      Left            =   0
      TabIndex        =   1
      Top             =   1560
      Width           =   5415
   End
   Begin VB.Label labServerBuild 
      Alignment       =   2  'Center
      Caption         =   "Server Build"
      Height          =   255
      Left            =   0
      TabIndex        =   0
      Top             =   1920
      Width           =   5415
   End
End
Attribute VB_Name = "frmAbout"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub CloseForm()
    Unload Me
End Sub

Public Sub SaveSettings()

End Sub

Private Sub Form_Load()
    ResizeChildWindow Me.hwnd, frmMain.picMain.hwnd
    SetWindowTitle "About XGS/32"
    labClientBuild = "Client Build: " & App.Major & "." & App.Minor & "." & App.Revision
    labServerBuild = "Server Build: " & g_objXGSServer.Version
End Sub

Private Sub Form_Resize()
    Dim ctl As Control
    Dim rRect As RECT
    On Error Resume Next
    GetClientRect Me.hwnd, rRect
    For Each ctl In Me.Controls
        ctl.Left = 1
        ctl.Width = rRect.Right
    Next
End Sub
