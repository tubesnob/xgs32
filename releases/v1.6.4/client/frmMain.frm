VERSION 5.00
Object = "{065E6FD1-1BF9-11D2-BAE8-00104B9E0792}#3.0#0"; "ssa3d30.ocx"
Begin VB.Form frmMain 
   Caption         =   "XGS/32"
   ClientHeight    =   7200
   ClientLeft      =   165
   ClientTop       =   450
   ClientWidth     =   9555
   Icon            =   "frmMain.frx":0000
   LinkTopic       =   "Form1"
   ScaleHeight     =   480
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   637
   StartUpPosition =   3  'Windows Default
   Begin VB.Timer Timer1 
      Left            =   8280
      Top             =   3000
   End
   Begin VB.PictureBox picMain 
      BackColor       =   &H8000000C&
      Height          =   3135
      Left            =   1320
      ScaleHeight     =   205
      ScaleMode       =   3  'Pixel
      ScaleWidth      =   381
      TabIndex        =   11
      Top             =   1560
      Width           =   5775
   End
   Begin Threed.SSPanel ssPanel 
      Align           =   3  'Align Left
      Height          =   7200
      Left            =   0
      TabIndex        =   0
      Top             =   0
      Width           =   855
      _ExtentX        =   1508
      _ExtentY        =   12700
      _Version        =   196609
      ActiveColors    =   -1  'True
      Alignment       =   0
      RoundedCorners  =   0   'False
      FloodShowPct    =   -1  'True
      Begin Threed.SSRibbon ssButtons 
         Height          =   540
         Index           =   4
         Left            =   120
         TabIndex        =   5
         ToolTipText     =   "Video Configuration"
         Top             =   3000
         Width           =   540
         _ExtentX        =   953
         _ExtentY        =   953
         _Version        =   196609
         PictureFrames   =   1
         Picture         =   "frmMain.frx":08CA
         AutoSize        =   2
         ButtonStyle     =   3
      End
      Begin Threed.SSRibbon ssButtons 
         Height          =   540
         Index           =   2
         Left            =   120
         TabIndex        =   3
         ToolTipText     =   "3.5"" and 5.25"" Disk Images (Slots 5 and 6)"
         Top             =   1560
         Width           =   540
         _ExtentX        =   953
         _ExtentY        =   953
         _Version        =   196609
         PictureFrames   =   1
         Picture         =   "frmMain.frx":15A4
         AutoSize        =   2
         ButtonStyle     =   3
      End
      Begin Threed.SSRibbon ssButtons 
         Height          =   540
         Index           =   1
         Left            =   120
         TabIndex        =   2
         ToolTipText     =   "SMPT Disk Images (3.5"" and HDV)"
         Top             =   840
         Width           =   540
         _ExtentX        =   953
         _ExtentY        =   953
         _Version        =   196609
         PictureFrames   =   1
         Picture         =   "frmMain.frx":227E
         AutoSize        =   2
         ButtonStyle     =   3
      End
      Begin Threed.SSRibbon ssButtons 
         Height          =   540
         Index           =   3
         Left            =   120
         TabIndex        =   4
         ToolTipText     =   "Hardware Configuration"
         Top             =   2280
         Width           =   540
         _ExtentX        =   953
         _ExtentY        =   953
         _Version        =   196609
         PictureFrames   =   1
         Picture         =   "frmMain.frx":2F58
         AutoSize        =   2
         ButtonStyle     =   3
      End
      Begin Threed.SSRibbon ssButtons 
         Height          =   540
         Index           =   5
         Left            =   120
         TabIndex        =   6
         ToolTipText     =   "Audio/Sound Configuration"
         Top             =   3720
         Width           =   540
         _ExtentX        =   953
         _ExtentY        =   953
         _Version        =   196609
         PictureFrames   =   1
         Picture         =   "frmMain.frx":3C32
         AutoSize        =   2
         ButtonStyle     =   3
      End
      Begin Threed.SSRibbon ssButtons 
         Height          =   540
         Index           =   6
         Left            =   120
         TabIndex        =   7
         ToolTipText     =   "Joystick Configuration"
         Top             =   4440
         Width           =   540
         _ExtentX        =   953
         _ExtentY        =   953
         _Version        =   196609
         PictureFrames   =   1
         Picture         =   "frmMain.frx":490C
         AutoSize        =   2
         ButtonStyle     =   3
      End
      Begin Threed.SSRibbon ssButtons 
         Height          =   540
         Index           =   0
         Left            =   120
         TabIndex        =   1
         ToolTipText     =   "Run the Emulator!"
         Top             =   120
         Width           =   540
         _ExtentX        =   953
         _ExtentY        =   953
         _Version        =   196609
         PictureFrames   =   1
         Picture         =   "frmMain.frx":55E6
         AutoSize        =   2
         ButtonStyle     =   3
      End
      Begin Threed.SSRibbon ssButtons 
         Height          =   540
         Index           =   9
         Left            =   120
         TabIndex        =   10
         ToolTipText     =   "About XGS/32"
         Top             =   6600
         Width           =   540
         _ExtentX        =   953
         _ExtentY        =   953
         _Version        =   196609
         PictureFrames   =   1
         Picture         =   "frmMain.frx":5EC0
         AutoSize        =   2
         ButtonStyle     =   3
      End
      Begin Threed.SSRibbon ssButtons 
         Height          =   540
         Index           =   8
         Left            =   120
         TabIndex        =   9
         ToolTipText     =   "XGS/32 Help and Documentation"
         Top             =   5880
         Width           =   540
         _ExtentX        =   953
         _ExtentY        =   953
         _Version        =   196609
         PictureFrames   =   1
         Picture         =   "frmMain.frx":6B9A
         AutoSize        =   2
         ButtonStyle     =   3
      End
      Begin Threed.SSRibbon ssButtons 
         Height          =   540
         Index           =   7
         Left            =   120
         TabIndex        =   8
         ToolTipText     =   "Communications Configuration"
         Top             =   5160
         Width           =   540
         _ExtentX        =   953
         _ExtentY        =   953
         _Version        =   196609
         PictureFrames   =   1
         Picture         =   "frmMain.frx":7874
         AutoSize        =   2
         ButtonStyle     =   3
      End
   End
   Begin Threed.SSPanel Title 
      Height          =   450
      Left            =   825
      TabIndex        =   12
      Top             =   0
      Width           =   8700
      _ExtentX        =   15346
      _ExtentY        =   794
      _Version        =   196609
      Font3D          =   5
      ForeColor       =   8454143
      BackColor       =   16744576
      BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
         Name            =   "Lucida Console"
         Size            =   9
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Caption         =   "XGS/32"
      BevelWidth      =   2
      BorderWidth     =   1
      RoundedCorners  =   0   'False
      FloodShowPct    =   -1  'True
      Begin Threed.SSPanel ssStatus 
         Height          =   270
         Left            =   6195
         TabIndex        =   13
         Top             =   105
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   476
         _Version        =   196609
         MarqueeStyle    =   4
         ForeColor       =   0
         BackColor       =   0
         MarqueeScrollAmount=   3
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Lucida Console"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "ssStatus"
         BevelOuter      =   1
         RoundedCorners  =   0   'False
         FloodShowPct    =   -1  'True
      End
      Begin Threed.SSPanel ssSpeed 
         Height          =   270
         Left            =   7425
         TabIndex        =   14
         Top             =   105
         Width           =   1200
         _ExtentX        =   2117
         _ExtentY        =   476
         _Version        =   196609
         ForeColor       =   8454143
         BackColor       =   0
         BeginProperty Font {0BE35203-8F91-11CE-9DE3-00AA004BB851} 
            Name            =   "Lucida Console"
            Size            =   8.25
            Charset         =   0
            Weight          =   700
            Underline       =   0   'False
            Italic          =   0   'False
            Strikethrough   =   0   'False
         EndProperty
         Caption         =   "ssSpeed"
         BevelOuter      =   1
         RoundedCorners  =   0   'False
         FloodShowPct    =   -1  'True
      End
   End
End
Attribute VB_Name = "frmMain"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

' XGS/32 Visual Basic Front End Demo
'
' Copyright (c) 1998, Mudflap! Software
'
' Nuf Said
'''''''''''''''''''''''''''''''''''''''''''''''''''

Option Explicit

Dim objVisibleForm As Object
Dim iLastButton As Integer
Dim objHelp As HTMLHelp

Private Sub Timer1_Timer()
    
    Dim lColor As Long
    Dim strStateText As String
    On Error Resume Next
    
    If g_objXGSServer Is Nothing Then
        Exit Sub
    End If
        
    Select Case g_objXGSServer.RunState
        
        Case 0: strStateText = "Running"
                lColor = vbGreen
        Case 1: lColor = vbGreen
                strStateText = "Idle"
        Case 2: lColor = vbYellow
                strStateText = "Paused"
        Case 4: lColor = vbBlue
                strStateText = "Pausing"
        Case 8: lColor = vbBlue
                strStateText = "Terminating"
        Case 16: lColor = vbRed
                strStateText = "Pausing"
    End Select
    frmMain.Caption = g_GETVERSIONINFO & "   [" & strStateText & "]"
    
    ssStatus.Caption = strStateText
    ssStatus.ForeColor = lColor
    
    If (g_objXGSServer.RunState = 0) Then
        If ((iLastButton <> BUTTON_STARTXGS) And (Not g_lDisplayVideoInClient)) Then
        Else
            ssSpeed.Caption = Format(g_objXGSServer.AverageRunSpeed, "##0.00") & "Mhz"
        End If
    Else
        If objVisibleForm Is Nothing Then
            frmMain.Title.Caption = "XGS/32"
        End If
        ssSpeed.Caption = "N/A"
    End If
    
    
    
End Sub


Public Sub ResizeForEmulator()
        
        Dim rMain As RECT
        Dim rWnd As RECT
        Dim rPic As RECT
        
        GetClientRect frmMain.hwnd, rMain
        GetClientRect frmMain.picMain.hwnd, rPic
        
        Dim MainX As Integer
        Dim MainY As Integer
        
        MainX = frmMain.ssPanel.Width + g_lVideoWidth
        MainY = g_lVideoHeight + frmMain.Title.Height
        
        GetWindowRect frmMain.hwnd, rWnd
        
        Dim iTrueX, iTrueY, iXDelta, iYDelta As Integer
        
        iTrueX = rWnd.Right - rWnd.Left
        iTrueY = rWnd.Bottom - rWnd.Top
        iXDelta = iTrueX - rMain.Right
        iYDelta = iTrueY - rMain.Bottom
            
        MainX = MainX + iXDelta
        MainY = MainY + iYDelta
        
        SetWindowPos frmMain.hwnd, 0, 0, 0, MainX, MainY, 0
        
        If Not objVisibleForm Is Nothing Then
            ResizeChildWindow objVisibleForm.hwnd, frmMain.picMain.hwnd
        End If
        
        DoEvents
        
End Sub

Public Sub ResizeForClient()
End Sub

Private Sub Form_Load()
    
    On Error GoTo HandleError:
    
    ' Load the user defaults..
    LoadRegDefaults
    
    ' Create a server reference
    Set g_objXGSServer = New XGS32Lib.XGS32Obj
    
    ' make the about screen the opener...
    'ssButtons_Click BUTTON_ABOUT, 0
    
    Timer1.Interval = 250
    Timer1.Enabled = True
    
    Exit Sub

HandleError:
    If g_objXGSServer Is Nothing Then
        MsgBox "An error occurred when trying to create the XGS/32 Server." & vbCrLf & vbCrLf & "This may be due to an installation failure, or the component may not be registered properly. Please reinstall the software, or send email to smentzer@pacbell.net", vbCritical + vbOKOnly, "XGS/32 Error! :("
    Else
        MsgBox "An unspecified error occurred when trying to load the application. Please send email to smentzer@pacbell.net", vbCritical + vbOKOnly, "XGS/32 Error! :("
    End If
    Unload Me
End Sub

Private Sub Form_Resize()
    Dim rMain As RECT
    On Error Resume Next
    With Me
        GetClientRect .hwnd, rMain
        .Title.Left = .ssPanel.Width
        .Title.Width = rMain.Right - .ssPanel.Width - 1
        .Title.Top = 0
        ssSpeed.Left = ScaleX(.Title.Width, vbPixels, vbTwips) - ssSpeed.Width - ScaleX(3, vbPixels, vbTwips)
        ssStatus.Left = .ssSpeed.Left - ScaleX(2, vbPixels, vbTwips) - ssStatus.Width
        .picMain.Left = .ssPanel.Width
        .picMain.Top = .Title.Height
        .picMain.Width = rMain.Right - .ssPanel.Width
        .picMain.Height = rMain.Bottom - .Title.Height
    End With
End Sub

Private Sub Form_Unload(Cancel As Integer)
    If Not (objVisibleForm Is Nothing) Then
        objVisibleForm.SaveSettings
        objVisibleForm.CloseForm
        Set objVisibleForm = Nothing
    End If
    ' Unload our server component
    If Not g_objXGSServer Is Nothing Then
        g_objXGSServer.Shutdown
        Set g_objXGSServer = Nothing
    End If
End Sub


Private Sub ssButtons_Click(Index As Integer, Value As Integer)
    Dim objForm As Object
    
    If Value = 0 Then
        Exit Sub
    End If
    
    If g_objXGSServer.RunState = 0 Then
        If g_lDisplayVideoInClient Then
            g_objXGSServer.EmulatorPause
        End If
        ResizeForClient
    End If
    
    If ((iLastButton = BUTTON_STARTXGS) And (Not objVisibleForm Is Nothing)) Then
        objVisibleForm.Show
    End If
    
    If Not (objVisibleForm Is Nothing) Then
        objVisibleForm.SaveSettings
        objVisibleForm.CloseForm
        Set objVisibleForm = Nothing
    End If
    
    
    Select Case (Index)
        Case BUTTON_STARTXGS
            'Set objForm = New frmStartXGS
            If g_objXGSServer.RunState <> 0 Then
                RunEmulator
            End If
        Case BUTTON_SMPTIMAGES
            Set objForm = New frmSMPTDisks
        Case BUTTON_SLOTIMAGES
            Set objForm = New frmSlotDisks
        Case BUTTON_HARDWARE
            Set objForm = New frmHardwareConfiguration
        Case BUTTON_VIDEO
            Set objForm = New frmVideoConfiguration
        Case BUTTON_AUDIO
            Set objForm = New frmAudioConfiguration
        Case BUTTON_JOYSTICK
            Set objForm = New frmJoystickConfiguration
        Case BUTTON_COMM
            Set objForm = New frmCommConfiguration
        Case BUTTON_ABOUT
            Set objForm = New frmAbout
        Case BUTTON_HELP
            'Set objForm = New frmHelp
            DisplayHelp
            Value = 0
    End Select
    iLastButton = Index
    If ((Index <> BUTTON_STARTXGS) And (Index <> BUTTON_HELP)) Then
        Set objVisibleForm = objForm
        SetParent objForm.hwnd, picMain.hwnd
        objForm.Show
    End If
End Sub

Public Sub DisplayHelp()
    Dim strHelpFile As String
    On Error GoTo HandleError:
    strHelpFile = App.Path & "\xgs32help.chm"
    Set objHelp = New HTMLHelp
    objHelp.CHMFile = strHelpFile
    objHelp.HHDisplayContents
    Exit Sub
HandleError:
    MsgBox "XGS/32 was unable to display the helpfile located at '" & strHelpFile & "'. Please make sure that the file exists at that location.", vbCritical
End Sub

Private Sub RunEmulator()
    
    Dim i As Integer
    Dim j As Integer
    Dim x As Single
    Dim y As Single
    Dim lHwnd As Long
    
    On Error GoTo HandleError:
    
    If ((g_lVideoHeight = 0) Or (g_lVideoWidth = 0)) Then
        MsgBox "You need to select a video mode before you can run the emulator. Click on ""Options->Emulator Configuration"" and select the ""Video"" tab.", vbCritical, "XGS/32"
        Exit Sub
    End If
    
    If (g_lDisplayVideoInClient) Then
        
        frmMain.ResizeForEmulator
        DoEvents
        lHwnd = frmMain.picMain.hwnd
        
    Else
        lHwnd = 0
    End If
    
    If Not (g_objXGSServer Is Nothing) Then
        
        With g_objXGSServer
    
            ' if we aren't running (ie. we are idle), the set the initial parameters
            If .RunState = 1 Then
                
                ' initialize the emulator state
                .Initialize
                
                ' set our support files and hardware configuration
                .RAMSize = g_lTotalRam
                .Font40File = g_strFont40File
                .Font80File = g_strFont80File
                .BRAMFile = g_strBRAMFile
                .ROMFile = g_strROMImageFile
                
                ' assign our smartport images
                For i = 0 To 7
                    If Trim(g_strSmartPorts(i)) <> "" Then
                        .SmartPort(i) = g_strSmartPorts(i)
                    End If
                Next
                
                For i = 5 To 6
                 For j = 1 To 2
                  .slot(i, j) = g_strSlots(i, j)
                 Next
                Next
                
            End If
            
            If g_iEnableSpeedThrottle Then
                .TargetRunSpeed = g_dTargetRunSpeed
            Else
                .TargetRunSpeed = 0
            End If
            
            .DebugMode = g_iEnableDebug
            .SoundMode = g_iSoundMode
            
            .SetVideoMode g_lVideoWidth, g_lVideoHeight, g_lVideoBitDepth, g_lVideoRefreshRate, g_lVidWindowed, lHwnd
            .TargetVideoRefreshRate = g_dTargetVideoRefreshRate
            .SoundOutputBuffers = g_lSoundOutputBuffers
            .SoundOutputBufferSize = g_lSoundOutputBufferSize
            .LockVideoUpdates = g_lLockVideoUpdates
            .EmulatorUpdateMode = g_lEmulatorUpdateMode
            .EmulatorUpdatePeriod = g_lEmulatorUpdatePeriod
            .JoystickMode = g_lJoystickMode
            .EmulatorRun
            
        End With
    
    End If
    
    Exit Sub

HandleError:

    App.LogEvent "frmMain->Run(): " & Err.Description
    Resume Next
    'MsgBox "An error occurred trying to load the XGS32 server. You propably haven't installed or the software properly. Please read the HOWTOINSTALL.TXT file located in the XGS32 directory.", vbOKOnly And vbCritical, "Error While Loading XGS/32!"
        
End Sub

