VERSION 5.00
Begin VB.Form frmJoystickConfiguration 
   AutoRedraw      =   -1  'True
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   3765
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6975
   LinkTopic       =   "Form1"
   ScaleHeight     =   251
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   465
   ShowInTaskbar   =   0   'False
   Begin VB.ComboBox lstJoystickMode 
      Height          =   315
      Left            =   2385
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   345
      Width           =   3585
   End
   Begin VB.Label Label3 
      Caption         =   "Joystick Mode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   345
      TabIndex        =   1
      Top             =   345
      Width           =   1935
   End
End
Attribute VB_Name = "frmJoystickConfiguration"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Public Sub CloseForm()
    Unload Me
End Sub

Public Sub SaveSettings()
    g_lJoystickMode = lstJoystickMode.ListIndex
    SaveRegDefaults
End Sub

Private Sub Form_Load()
    ResizeChildWindow Me.hwnd, frmMain.picMain.hwnd
    SetWindowTitle "Joystick Configuration"
    lstJoystickMode.Clear
    lstJoystickMode.AddItem "Disabled [Virtual Joystick via F5]"
    lstJoystickMode.AddItem "Enabled [Win32 API Mode]"
    lstJoystickMode.AddItem "Enabled [DirectInput Mode]"
    lstJoystickMode.ListIndex = g_lJoystickMode
End Sub

