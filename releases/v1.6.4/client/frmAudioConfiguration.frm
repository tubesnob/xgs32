VERSION 5.00
Begin VB.Form frmAudioConfiguration 
   AutoRedraw      =   -1  'True
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   5100
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   10260
   LinkTopic       =   "Form1"
   ScaleHeight     =   340
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   684
   ShowInTaskbar   =   0   'False
   Begin VB.TextBox edtMaxLag 
      Enabled         =   0   'False
      Height          =   375
      Left            =   2400
      TabIndex        =   7
      Top             =   3120
      Width           =   2895
   End
   Begin VB.TextBox edtSoundOutputBuffers 
      Height          =   375
      Left            =   2400
      TabIndex        =   4
      Top             =   2280
      Width           =   2895
   End
   Begin VB.TextBox edtSoundOutputBufferSize 
      Height          =   375
      Left            =   2400
      TabIndex        =   1
      Top             =   960
      Width           =   2895
   End
   Begin VB.ComboBox lstAudioMode 
      Height          =   315
      Left            =   2400
      TabIndex        =   0
      Text            =   "Combo1"
      Top             =   360
      Width           =   2895
   End
   Begin VB.Label Label1 
      Caption         =   "Maximum Possible Audio Lag"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   360
      TabIndex        =   8
      Top             =   3120
      Width           =   2055
   End
   Begin VB.Label Label4 
      Caption         =   "The emulator core runs audio at 28000Hz. A Value of 2800 here would render each buffer to be 1/10 of a second."
      BeginProperty Font 
         Name            =   "Arial"
         Size            =   6.75
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   735
      Left            =   2400
      TabIndex        =   6
      Top             =   1440
      Width           =   2895
   End
   Begin VB.Label Label9 
      Caption         =   "Number Of Audio Buffers"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   360
      TabIndex        =   5
      Top             =   2280
      Width           =   2055
   End
   Begin VB.Label Label10 
      Caption         =   "Audio Buffer Size"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   3
      Top             =   960
      Width           =   1935
   End
   Begin VB.Label Label3 
      Caption         =   "Audio Mode"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   360
      TabIndex        =   2
      Top             =   360
      Width           =   1935
   End
End
Attribute VB_Name = "frmAudioConfiguration"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub RefreshForm()
    Dim lBufferSize As Long
    Dim lBuffers As Long
    Dim dLag As Double
    lBuffers = Val(edtSoundOutputBuffers.Text)
    lBufferSize = Val(edtSoundOutputBufferSize.Text)
    
    If (lBuffers > 0 And lBufferSize > 0) Then
        dLag = (Val(edtSoundOutputBufferSize) * Val(edtSoundOutputBuffers)) / 28000
        edtMaxLag.Text = dLag & " second(s)"
    Else
        edtMaxLag.Text = "N/A"
    End If
    
    
End Sub

Private Sub edtSoundOutputBuffers_Change()
    RefreshForm
End Sub

Private Sub edtSoundOutputBuffers_LostFocus()
    RefreshForm
End Sub

Private Sub edtSoundOutputBufferSize_Change()
    RefreshForm
End Sub

Private Sub edtSoundOutputBufferSize_LostFocus()
    RefreshForm
End Sub

Private Sub Form_Load()
    ResizeChildWindow Me.hwnd, frmMain.picMain.hwnd
    SetWindowTitle "Audio Configuration"
    lstAudioMode.Clear
    lstAudioMode.AddItem "Disabled"
    lstAudioMode.AddItem "Ensoniq Enabled. No Windows Output"
    lstAudioMode.AddItem "Enabled"
    lstAudioMode.ListIndex = g_iSoundMode
    edtSoundOutputBuffers = g_lSoundOutputBuffers
    edtSoundOutputBufferSize = g_lSoundOutputBufferSize
    RefreshForm
End Sub

Public Sub CloseForm()
    Unload Me
End Sub

Public Sub SaveSettings()
    g_iSoundMode = lstAudioMode.ListIndex
    g_lSoundOutputBufferSize = Val(edtSoundOutputBufferSize)
    g_lSoundOutputBuffers = Val(edtSoundOutputBuffers)
    SaveRegDefaults
End Sub


Private Sub lstAudioMode_Change()
    RefreshForm
End Sub
