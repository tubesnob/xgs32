VERSION 5.00
Begin VB.Form frmCommConfiguration 
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   3195
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   4680
   LinkTopic       =   "Form1"
   ScaleHeight     =   213
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   312
   ShowInTaskbar   =   0   'False
   StartUpPosition =   3  'Windows Default
   Begin VB.Label Label1 
      Alignment       =   2  'Center
      Caption         =   "Coming Soon!"
      BeginProperty Font 
         Name            =   "MS Sans Serif"
         Size            =   24
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   855
      Left            =   600
      TabIndex        =   0
      Top             =   720
      Width           =   3735
   End
End
Attribute VB_Name = "frmCommConfiguration"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub CloseForm()
    Unload Me
End Sub

Public Sub SaveSettings()
    SaveRegDefaults
End Sub

Private Sub Form_Load()
    ResizeChildWindow Me.hwnd, frmMain.picMain.hwnd
    SetWindowTitle "Communications/Printer Configuration"
End Sub

Private Sub Form_Resize()
    Dim ctl As Control
    Dim rRect As RECT
    On Error Resume Next
    GetClientRect Me.hwnd, rRect
    For Each ctl In Me.Controls
        ctl.Left = 1
        ctl.Width = rRect.Right
    Next
End Sub

