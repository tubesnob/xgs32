VERSION 5.00
Object = "{F9043C88-F6F2-101A-A3C9-08002B2F49FB}#1.2#0"; "COMDLG32.OCX"
Begin VB.Form frmSlotDisks 
   AutoRedraw      =   -1  'True
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   3825
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   5760
   LinkTopic       =   "Form1"
   ScaleHeight     =   255
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   384
   ShowInTaskbar   =   0   'False
   Begin VB.CommandButton ChangeSlot 
      Height          =   375
      Index           =   3
      Left            =   4680
      Picture         =   "frmSlotDisks.frx":0000
      Style           =   1  'Graphical
      TabIndex        =   11
      Top             =   1800
      Width           =   375
   End
   Begin VB.CommandButton ChangeSlot 
      Height          =   375
      Index           =   2
      Left            =   4680
      Picture         =   "frmSlotDisks.frx":0532
      Style           =   1  'Graphical
      TabIndex        =   10
      Top             =   1320
      Width           =   375
   End
   Begin VB.CommandButton ChangeSlot 
      Height          =   375
      Index           =   1
      Left            =   4680
      Picture         =   "frmSlotDisks.frx":0A64
      Style           =   1  'Graphical
      TabIndex        =   9
      Top             =   840
      Width           =   375
   End
   Begin VB.CommandButton ChangeSlot 
      Height          =   375
      Index           =   0
      Left            =   4680
      Picture         =   "frmSlotDisks.frx":0F96
      Style           =   1  'Graphical
      TabIndex        =   8
      Top             =   360
      Width           =   375
   End
   Begin VB.TextBox edtSlot 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   3
      Left            =   1320
      TabIndex        =   7
      Top             =   1800
      Width           =   3135
   End
   Begin VB.TextBox edtSlot 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   2
      Left            =   1320
      TabIndex        =   6
      Top             =   1320
      Width           =   3135
   End
   Begin VB.TextBox edtSlot 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   1
      Left            =   1320
      TabIndex        =   5
      Top             =   840
      Width           =   3135
   End
   Begin VB.TextBox edtSlot 
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   8.25
         Charset         =   0
         Weight          =   400
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Index           =   0
      Left            =   1320
      TabIndex        =   4
      Top             =   360
      Width           =   3135
   End
   Begin MSComDlg.CommonDialog dlgFileOpen 
      Left            =   5160
      Top             =   360
      _ExtentX        =   847
      _ExtentY        =   847
      _Version        =   393216
   End
   Begin VB.Label Label11 
      Caption         =   "S5 D2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   3
      Top             =   840
      Width           =   615
   End
   Begin VB.Label Label14 
      Caption         =   "S6 D2"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   2
      Top             =   1800
      Width           =   615
   End
   Begin VB.Label Label13 
      Caption         =   "S6 D1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   1
      Top             =   1320
      Width           =   615
   End
   Begin VB.Label Label10 
      Caption         =   "S5 D1"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   0
      Top             =   360
      Width           =   615
   End
End
Attribute VB_Name = "frmSlotDisks"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub SaveSettings()
    SaveRegDefaults
End Sub

Public Sub CloseForm()
    Unload Me
End Sub

Private Sub LoadDrive(slot As Integer, drive As Integer, editbox As Control)
    If ((g_objXGSServer.RunState = 2) Or (g_objXGSServer.RunState = 0)) Then
        If (editbox.Text <> g_strSlots(slot, drive)) Then
            g_objXGSServer.slot(slot, drive) = editbox.Text
        End If
    End If
    g_strSlots(slot, drive) = editbox.Text
End Sub


Private Sub AssignSlots()
    LoadDrive 5, 1, edtSlot(0)
    LoadDrive 5, 2, edtSlot(1)
    LoadDrive 6, 1, edtSlot(2)
    LoadDrive 6, 2, edtSlot(3)
End Sub

Private Sub DisplaySlots()
    edtSlot(0) = g_strSlots(5, 1)
    edtSlot(1) = g_strSlots(5, 2)
    edtSlot(2) = g_strSlots(6, 1)
    edtSlot(3) = g_strSlots(6, 2)
End Sub

Private Sub ChangeSlot_Click(Index As Integer)
    On Error GoTo UserCancel
    dlgFileOpen.CancelError = True
    dlgFileOpen.Filter = "All files (*.*)|*.*|2MG Images (*.2mg)|*.2mg|DSK Images(*.dsk)|*.dsk|Dos Order Image (*.do)|*.do|ProDos Order Images (*.po)|*.po|Old-Style XGS Images (*.xgs)|*.xgs"
    dlgFileOpen.FilterIndex = 2
    dlgFileOpen.DialogTitle = "Select Slot Disk Image"
    dlgFileOpen.DefaultExt = "*.2mg"
    dlgFileOpen.ShowOpen
    edtSlot(Index).Text = dlgFileOpen.FileName
    AssignSlots
    Exit Sub
UserCancel:
    Exit Sub
End Sub

Private Sub edtSlot_LostFocus(Index As Integer)
    AssignSlots
End Sub

Private Sub Form_Load()
    ResizeChildWindow Me.hwnd, frmMain.picMain.hwnd
    SetWindowTitle "Slot based disk-image assignments"
    DisplaySlots
End Sub

Private Sub Form_Unload(Cancel As Integer)
    SaveRegDefaults
End Sub
