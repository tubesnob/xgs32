Attribute VB_Name = "globals"
' XGS/32 Visual Basic Front End Demo
'
' Copyright (c) 1998, Mudflap! Software
'
' Nuf Said
'''''''''''''''''''''''''''''''''''''''''''''''''''

Option Explicit


Public Type RECT
        Left As Long
        Top As Long
        Right As Long
        Bottom As Long
End Type
Public Declare Function GetClientRect Lib "user32" (ByVal hwnd As Long, lpRect As RECT) As Long
Public Declare Sub Sleep Lib "kernel32" (ByVal dwMilliseconds As Long)
Public Declare Function SetParent Lib "user32" (ByVal hWndChild As Long, ByVal hWndNewParent As Long) As Long
Public Declare Function SetWindowPos Lib "user32" (ByVal hwnd As Long, ByVal hWndInsertAfter As Long, ByVal x As Long, ByVal y As Long, ByVal cx As Long, ByVal cy As Long, ByVal wFlags As Long) As Long
Public Declare Function LockWindowUpdate Lib "user32" (ByVal hwndLock As Long) As Long
Public Declare Function GetWindowRect Lib "user32" (ByVal hwnd As Long, lpRect As RECT) As Long

Global Const BUTTON_STARTXGS = 0
Global Const BUTTON_SMPTIMAGES = 1
Global Const BUTTON_SLOTIMAGES = 2
Global Const BUTTON_HARDWARE = 3
Global Const BUTTON_VIDEO = 4
Global Const BUTTON_AUDIO = 5
Global Const BUTTON_JOYSTICK = 6
Global Const BUTTON_COMM = 7
Global Const BUTTON_HELP = 8
Global Const BUTTON_ABOUT = 9
        
Global g_objXGSServer As XGS32Lib.XGS32Obj

Global g_iSoundMode As Integer
Global g_lSoundOutputBufferSize As Long
Global g_lSoundOutputBuffers As Long

Global g_iEnableDebug As Integer
Global g_dTargetRunSpeed As Double
Global g_iEnableSpeedThrottle As Integer

Global g_lTotalRam As Long
Global g_strSmartPorts(16) As String
Global g_strSlots(8, 3) As String
Global g_strROMImageFile As String
Global g_strFont40File As String
Global g_strFont80File As String
Global g_strBRAMFile As String

Global g_strInstallPath As String

Global g_lVideoWidth As Long
Global g_lVideoHeight As Long
Global g_lVideoBitDepth As Long
Global g_lVideoRefreshRate As Long
Global g_lVidWindowed As Long
Global g_dTargetVideoRefreshRate As Double
Global g_lLockVideoUpdates As Long
Global g_lDisplayVideoInClient As Long

Global g_strSelectedJoystick As String

Global g_lEmulatorUpdateMode As Long
Global g_lEmulatorUpdatePeriod As Long

Global g_lJoystickMode As Long


Global Const C_THISVER = "1.6.3"

Private Sub DisplayVersionMessage()
    Dim strMessage As String
    strMessage = "FYI: As of version 1.6.3 *beta*, you are now *required* to press CTRL-F12 to terminate the emulator."
    strMessage = strMessage & vbCrLf & vbCrLf & "Please read the online documentation section 'Keyboard Commands' for more details"
    MsgBox strMessage, vbApplicationModal Or vbInformation Or vbOKOnly, "XGS/32 FYI :)"
    SaveSetting "XGS32", "Version", "LastVersion", C_THISVER
End Sub

Public Sub CheckInitialRegistryEntries()
    Dim iFirstRun As Integer
    Dim strLastVersion As String
    strLastVersion = GetSetting("XGS32", "Version", "LastVersion", "")
    If strLastVersion < C_THISVER Then
        DisplayVersionMessage
    End If
    
    iFirstRun = GetSetting("XGS32", "Parameters", "FirstRun", 1)
    If iFirstRun > 0 Then
        SaveSetting "XGS32", "Parameters", "ROMImageFile", "xgs.rom"
        SaveSetting "XGS32", "Parameters", "InstallPath", App.Path
        SaveSetting "XGS32", "Parameters", "BRAMFile", "xgs.ram"
        SaveSetting "XGS32", "Parameters", "Font40File", "xgs40.fnt"
        SaveSetting "XGS32", "Parameters", "Font80File", "xgs80.fnt"
        SaveSetting "XGS32", "Parameters", "EnableDebug", 0
        SaveSetting "XGS32", "Parameters", "EnableSound", 0
        SaveSetting "XGS32", "Parameters", "TotalRAM", 4
        SaveSetting "XGS32", "Parameters", "EnableSpeedThrottle", 0
        SaveSetting "XGS32", "Parameters", "TargetRunSpeed", 0
        SaveSetting "XGS32", "Parameters", "KeyboardRepeatDelay", 250
        SaveSetting "XGS32", "Parameters", "KeyboardRepeatRate", 10
        SaveSetting "XGS32", "Video", "Height", 640
        SaveSetting "XGS32", "Video", "Width", 480
        SaveSetting "XGS32", "Video", "BitDepth", 8
        SaveSetting "XGS32", "Video", "LockVideoUpdates", 0
        SaveSetting "XGS32", "Video", "DisplayVideoInClientArea", 0
        SaveSetting "XGS32", "Video", "RefreshRate", 60
        SaveSetting "XGS32", "Parameters", "FirstRun", 0
        SaveSetting "XGS32", "Video", "Windowed", 0
        SaveSetting "XGS32", "Joystick", "JoystickMode", 0
        SaveSetting "XGS32", "Video", "TargetVideoRefreshRate", 60
        SaveSetting "XGS32", "Sound", "SoundOutputBuffers", 32
        SaveSetting "XGS32", "Sound", "SoundOutputBufferSize", 2800
        SaveSetting "XGS32", "Parameters", "EmulatorUpdateMode", 0
        SaveSetting "XGS32", "Parameters", "EmulatorUpdatePeriod", 100
    End If
End Sub

Public Sub LoadRegDefaults()
    Dim i As Integer
    Dim j As Integer
    Dim strparm As String
    On Error Resume Next
    CheckInitialRegistryEntries
    g_strROMImageFile = GetSetting("XGS32", "Parameters", "ROMImageFile", "e:\xgs32\rom3.rom")
    g_strBRAMFile = GetSetting("XGS32", "Parameters", "BRAMFile", "e:\xgs32\xgs.ram")
    g_strFont40File = GetSetting("XGS32", "Parameters", "Font40File", "e:\xgs32\xgs40.fnt")
    g_strFont80File = GetSetting("XGS32", "Parameters", "Font80File", "e:\xgs32\xgs80.fnt")
    g_iEnableDebug = GetSetting("XGS32", "Parameters", "EnableDebug", 0)
    g_iSoundMode = GetSetting("XGS32", "Parameters", "EnableSound", 0)
    g_lTotalRam = GetSetting("XGS32", "Parameters", "TotalRAM", 0)
    g_iEnableSpeedThrottle = GetSetting("XGS32", "Parameters", "EnableSpeedThrottle", 0)
    g_dTargetRunSpeed = GetSetting("XGS32", "Parameters", "TargetRunSpeed", 7)
    g_lVideoHeight = GetSetting("XGS32", "Video", "Height", 0)
    g_lVideoWidth = GetSetting("XGS32", "Video", "Width", 0)
    g_lVideoBitDepth = GetSetting("XGS32", "Video", "BitDepth", 0)
    g_lVideoRefreshRate = GetSetting("XGS32", "Video", "RefreshRate", 0)
    g_dTargetVideoRefreshRate = GetSetting("XGS32", "Video", "TargetVideoRefreshRate", 60)
    g_lVidWindowed = GetSetting("XGS32", "Video", "Windowed", 0)
    g_strSelectedJoystick = GetSetting("XGS32", "Joystick", "DeviceGUID", "")
    g_lSoundOutputBuffers = GetSetting("XGS32", "Sound", "SoundOutputBuffers", 32)
    g_lSoundOutputBufferSize = GetSetting("XGS32", "Sound", "SoundOutputBufferSize", 2800)
    g_lLockVideoUpdates = GetSetting("XGS32", "Video", "LockVideoUpdates", 0)
    g_lEmulatorUpdateMode = GetSetting("XGS32", "Parameters", "EmulatorUpdateMode", 0)
    g_lEmulatorUpdatePeriod = GetSetting("XGS32", "Parameters", "EmulatorUpdatePeriod", 100)
    g_strInstallPath = GetSetting("XGS32", "Parameters", "InstallPath", App.Path)
    g_lDisplayVideoInClient = GetSetting("XGS32", "Video", "DisplayVideoInClientArea", 0)
    g_lJoystickMode = GetSetting("XGS32", "Joystick", "JoystickMode", 0)
    
    ' load smartport information
    For i = 0 To 15
        g_strSmartPorts(i) = GetSetting("XGS32", "Smartports", Trim(Str(i)), "")
    Next
    
    ' load slot information
    For i = 1 To 7
        For j = 1 To 2
            strparm = "S" & Trim(Str(i)) & "D" & Trim(Str(j))
            g_strSlots(i, j) = GetSetting("XGS32", "Slots", strparm, "")
        Next
    Next
    
End Sub

Public Sub SaveRegDefaults()
    Dim i As Integer
    Dim j As Integer
    Dim strparm As String
    On Error Resume Next
    SaveSetting "XGS32", "Parameters", "ROMImageFile", g_strROMImageFile
    SaveSetting "XGS32", "Parameters", "BRAMFile", g_strBRAMFile
    SaveSetting "XGS32", "Parameters", "Font40File", g_strFont40File
    SaveSetting "XGS32", "Parameters", "Font80File", g_strFont80File
    SaveSetting "XGS32", "Parameters", "EnableDebug", g_iEnableDebug
    SaveSetting "XGS32", "Parameters", "EnableSound", g_iSoundMode
    SaveSetting "XGS32", "Parameters", "TotalRAM", g_lTotalRam
    SaveSetting "XGS32", "Parameters", "EnableSpeedThrottle", g_iEnableSpeedThrottle
    SaveSetting "XGS32", "Parameters", "TargetRunSpeed", g_dTargetRunSpeed
    SaveSetting "XGS32", "Video", "Height", g_lVideoHeight
    SaveSetting "XGS32", "Video", "Width", g_lVideoWidth
    SaveSetting "XGS32", "Video", "BitDepth", g_lVideoBitDepth
    SaveSetting "XGS32", "Video", "RefreshRate", g_lVideoRefreshRate
    SaveSetting "XGS32", "Video", "Windowed", g_lVidWindowed
    SaveSetting "XGS32", "Joystick", "DeviceGUID", g_strSelectedJoystick
    SaveSetting "XGS32", "Video", "TargetVideoRefreshRate", g_dTargetVideoRefreshRate
    SaveSetting "XGS32", "Sound", "SoundOutputBuffers", g_lSoundOutputBuffers
    SaveSetting "XGS32", "Sound", "SoundOutputBufferSize", g_lSoundOutputBufferSize
    SaveSetting "XGS32", "Video", "LockVideoUpdates", g_lLockVideoUpdates
    SaveSetting "XGS32", "Video", "DisplayVideoInClientArea", g_lDisplayVideoInClient
    SaveSetting "XGS32", "Parameters", "EmulatorUpdateMode", g_lEmulatorUpdateMode
    SaveSetting "XGS32", "Parameters", "EmulatorUpdatePeriod", g_lEmulatorUpdatePeriod
    SaveSetting "XGS32", "Parameters", "InstallPath", g_strInstallPath
    SaveSetting "XGS32", "Joystick", "JoystickMode", g_lJoystickMode
    
    For i = 0 To 15
        If Trim(g_strSmartPorts(i)) <> "" Then
            SaveSetting "XGS32", "Smartports", Trim(Str(i)), g_strSmartPorts(i)
        Else
            DeleteSetting "XGS32", "Smartports", Trim(Str(i))
        End If
    Next
    
    For i = 1 To 7
        For j = 1 To 2
            strparm = "S" & Trim(Str(i)) & "D" & Trim(Str(j))
            If Trim(g_strSlots(i, j)) <> "" Then
                SaveSetting "XGS32", "Slots", strparm, g_strSlots(i, j)
            Else
                DeleteSetting "XGS32", "Slots", strparm
            End If
        Next
    Next
    
End Sub

Public Sub ResizeChildWindow(lChildHwnd As Long, lParentHwnd As Long)
    Dim lHR As Long
    Dim rParent As RECT
    GetClientRect lParentHwnd, rParent
    SetWindowPos lChildHwnd, 0, 0, 0, rParent.Right, rParent.Bottom, 0
End Sub

Public Sub SetWindowTitle(strTitle As String)
    Dim rRect As RECT
    With frmMain.Title
        .Caption = strTitle
    End With
End Sub

Function g_GETVERSIONINFO() As String
    g_GETVERSIONINFO = "XGS/32 v" & App.Major & "." & App.Minor & "." & App.Revision
End Function

