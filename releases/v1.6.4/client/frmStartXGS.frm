VERSION 5.00
Begin VB.Form frmStartXGS 
   AutoRedraw      =   -1  'True
   BorderStyle     =   0  'None
   Caption         =   "Start XGS"
   ClientHeight    =   4170
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   6015
   LinkTopic       =   "Form1"
   ScaleHeight     =   278
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   401
   ShowInTaskbar   =   0   'False
End
Attribute VB_Name = "frmStartXGS"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False

Public Sub CloseForm()
    Unload Me
End Sub

Public Sub SaveSettings()
    SaveRegDefaults
End Sub

Private Sub Form_Load()
    ResizeChildWindow Me.hwnd, frmMain.picMain.hwnd
    SetWindowTitle "Run XGS/32"
End Sub

Private Sub Form_Resize()
    Dim rRect As RECT
    On Error Resume Next
    GetClientRect Me.hwnd, rRect
    frameStats.Left = 1
    frameStats.Width = rRect.Right
End Sub



