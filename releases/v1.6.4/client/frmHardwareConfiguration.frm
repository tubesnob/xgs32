VERSION 5.00
Object = "{6B7E6392-850A-101B-AFC0-4210102A8DA7}#1.3#0"; "COMCTL32.OCX"
Begin VB.Form frmHardwareConfiguration 
   AutoRedraw      =   -1  'True
   BorderStyle     =   0  'None
   Caption         =   "Form1"
   ClientHeight    =   5850
   ClientLeft      =   0
   ClientTop       =   0
   ClientWidth     =   7620
   LinkTopic       =   "Form1"
   ScaleHeight     =   390
   ScaleMode       =   3  'Pixel
   ScaleWidth      =   508
   ShowInTaskbar   =   0   'False
   Begin VB.CheckBox chkEnableDebug 
      Caption         =   "Enable Debug Processing"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   345
      TabIndex        =   15
      Top             =   5190
      Width           =   3615
   End
   Begin VB.TextBox edtBRAMFile 
      Height          =   375
      Left            =   2160
      TabIndex        =   10
      Top             =   4680
      Width           =   3255
   End
   Begin VB.TextBox edtFont80File 
      Height          =   375
      Left            =   2160
      TabIndex        =   9
      Top             =   4200
      Width           =   3255
   End
   Begin VB.TextBox edtFont40File 
      Height          =   375
      Left            =   2160
      TabIndex        =   8
      Top             =   3720
      Width           =   3255
   End
   Begin VB.TextBox edtROMImageFile 
      Height          =   375
      Left            =   2160
      TabIndex        =   7
      Top             =   3240
      Width           =   3255
   End
   Begin VB.CommandButton cmdPerformSpeedTest 
      Caption         =   "Perform Speed Test"
      Height          =   375
      Left            =   600
      TabIndex        =   6
      Top             =   1560
      Width           =   2055
   End
   Begin VB.CheckBox chkEnableSpeedThrottle 
      Caption         =   "Throttle Emulator Speed"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   615
      Left            =   360
      TabIndex        =   1
      Top             =   240
      Width           =   2895
   End
   Begin VB.CheckBox chkFixedUpdatePeriod 
      Caption         =   "Fixed Update Period"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   495
      Left            =   360
      TabIndex        =   0
      Top             =   960
      Width           =   2535
   End
   Begin ComctlLib.Slider slSpeedThrottle 
      Height          =   375
      Left            =   3360
      TabIndex        =   2
      Top             =   360
      Width           =   4095
      _ExtentX        =   7223
      _ExtentY        =   661
      _Version        =   327682
      Max             =   250
      TickStyle       =   3
   End
   Begin ComctlLib.Slider slTotalRAM 
      Height          =   375
      Left            =   3360
      TabIndex        =   3
      Top             =   2400
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   661
      _Version        =   327682
      Min             =   1
      Max             =   8
      SelStart        =   1
      TickStyle       =   3
      Value           =   1
   End
   Begin ComctlLib.Slider slUpdatePeriod 
      Height          =   375
      Left            =   3360
      TabIndex        =   4
      Top             =   1080
      Width           =   3975
      _ExtentX        =   7011
      _ExtentY        =   661
      _Version        =   327682
      Min             =   1
      Max             =   1000
      SelStart        =   1
      TickStyle       =   3
      Value           =   1
   End
   Begin VB.Label Label8 
      Caption         =   "BRAM File"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   14
      Top             =   4680
      Width           =   1575
   End
   Begin VB.Label Label7 
      Caption         =   "80 Column Font"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   13
      Top             =   4200
      Width           =   1695
   End
   Begin VB.Label Label6 
      Caption         =   "40 Column Font"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   12
      Top             =   3720
      Width           =   1695
   End
   Begin VB.Label Label5 
      Caption         =   "ROM Image"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   375
      Left            =   360
      TabIndex        =   11
      Top             =   3240
      Width           =   1695
   End
   Begin VB.Label labTotalRAM 
      Caption         =   "Total RAM (MB)"
      BeginProperty Font 
         Name            =   "Tahoma"
         Size            =   9.75
         Charset         =   0
         Weight          =   700
         Underline       =   0   'False
         Italic          =   0   'False
         Strikethrough   =   0   'False
      EndProperty
      Height          =   255
      Left            =   360
      TabIndex        =   5
      Top             =   2400
      Width           =   2415
   End
End
Attribute VB_Name = "frmHardwareConfiguration"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Public Sub CloseForm()
    Unload Me
End Sub

Public Sub SaveSettings()
    g_lTotalRam = Val(slTotalRAM.Value)
    g_dTargetRunSpeed = slSpeedThrottle.Value / 10
    g_iEnableSpeedThrottle = chkEnableSpeedThrottle.Value
    g_strROMImageFile = edtROMImageFile
    g_strFont40File = edtFont40File
    g_strFont80File = edtFont80File
    g_strBRAMFile = edtBRAMFile
    g_lEmulatorUpdateMode = chkFixedUpdatePeriod.Value
    g_lEmulatorUpdatePeriod = slUpdatePeriod.Value
    g_iEnableDebug = chkEnableDebug.Value
    SaveRegDefaults
End Sub

Private Sub chkFixedUpdatePeriod_Click()
    RefreshForm
End Sub

Private Sub cmdPerformSpeedTest_Click()
    Dim iResponse As Integer
    Dim lTotalTime As Long
    Dim lOptimalTime As Long
    Dim j As Integer
    iResponse = MsgBox("The emulator will now perform a base speed test to determine the optimum update period. This process will take approximately 15 seconds.", vbApplicationModal Or vbOKCancel, "XGS/32 Emulator Speed Test")
    If iResponse = vbOK Then
    lTotalTime = 0
        For j = 1 To 5
            lTotalTime = lTotalTime + g_objXGSServer.LocalSpeedIndex
            Sleep 1000
        Next
        lOptimalTime = lTotalTime / 5
    End If
    MsgBox "The optimal update period for your computer is " & lOptimalTime & " cycles.", vbOKOnly
    slUpdatePeriod.Value = lOptimalTime
    RefreshForm
End Sub

Private Sub Form_Load()
    ResizeChildWindow Me.hwnd, frmMain.picMain.hwnd
    SetWindowTitle "Hardware Configuration"
    slTotalRAM.Value = g_lTotalRam
    chkEnableSpeedThrottle = g_iEnableSpeedThrottle
    slSpeedThrottle.Value = g_dTargetRunSpeed * 10
    chkEnableDebug = g_iEnableDebug
    edtROMImageFile = g_strROMImageFile
    edtFont40File = g_strFont40File
    edtFont80File = g_strFont80File
    edtBRAMFile = g_strBRAMFile
    slUpdatePeriod.Value = g_lEmulatorUpdatePeriod
    chkFixedUpdatePeriod.Value = g_lEmulatorUpdateMode
    chkEnableDebug.Value = g_iEnableDebug
    RefreshForm
End Sub


Private Sub chkEnableSpeedThrottle_Click()
    RefreshForm
End Sub

Public Sub RefreshForm()
    If slSpeedThrottle.Value Then
        chkEnableSpeedThrottle.Caption = " Throttle Emulator Speed (" & (Me.slSpeedThrottle.Value / 10) & "Mhz)"
    Else
        chkEnableSpeedThrottle.Caption = " Throttle Emulator Speed"
    End If
    labTotalRAM.Caption = " Total RAM (" & slTotalRAM.Value & "MB)"
    If chkEnableSpeedThrottle.Value Then
        slSpeedThrottle.Enabled = True
    Else
        slSpeedThrottle.Enabled = False
    End If
    If chkFixedUpdatePeriod.Value Then
        slUpdatePeriod.Enabled = True
        chkFixedUpdatePeriod.Caption = "Fixed Update Period (" & slUpdatePeriod.Value & " cycles)"
    Else
        slUpdatePeriod.Enabled = False
        chkFixedUpdatePeriod.Caption = "Fixed Update Period"
    End If
End Sub

Private Sub slSpeedThrottle_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button Then
        RefreshForm
    End If
End Sub

Private Sub slTotalRAM_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button Then
        RefreshForm
    End If
End Sub

Private Sub slUpdatePeriod_Click()
    RefreshForm
End Sub

Private Sub slUpdatePeriod_MouseMove(Button As Integer, Shift As Integer, x As Single, y As Single)
    If Button Then
        RefreshForm
    End If
End Sub

