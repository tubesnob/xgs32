#ifndef __VIDDRVUTILS__
#define __VIDDRVUTILS__

   #include "xgstypes.h"
   #include "ddraw.h"
   
   BOOL GetRGB16DEF(LPDIRECTDRAWSURFACE Surface);
   void WriteDDError(long errnum);
   HRESULT DDCreateOffscreenSurface(IDirectDraw *pdd, long DX, long DY, LPDIRECTDRAWSURFACE *ppdds, DDSURFACEDESC *pddsd);
   void VID_GenerateVidLines(int iType);
   void VID_GetDrawingSurface(LPDIRECTDRAWSURFACE *pDDSurface, DDSURFACEDESC **pDDSurfaceDesc);

#endif
