#ifndef __VIDDRVENUM__
#define __VIDDRVENUM__

   #include "xgstypes.h"

   int VID_GetDisplayModes(VARIANT *pvarDisplayModes);

#endif