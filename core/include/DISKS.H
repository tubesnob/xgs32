#ifndef _GSDISKS_H_
#define _GSDISKS_H_

   #include "xgstypes.h"

   extern   byte	dsk_buffer[512];

   void	   DSK_swapField(void *, int);
   int	   DSK_openImage(char *, disk_struct **);
   int	   DSK_closeImage(disk_struct *);
   int	   DSK_updateImage(disk_struct *);
   int	   DSK_readChunk(disk_struct *, byte *, int, int);
   int	   DSK_writeChunk(disk_struct *, byte *, int, int);
   int	   DSK_loadUnivImage(char *, disk_struct **, FILE *);
   int	   DSK_loadDosOrder(char *, disk_struct **, FILE *);
   int	   DSK_loadProdosOrder(char *, disk_struct **, FILE *);
   int	   DSK_loadNibblized(char *, disk_struct **, FILE *);
   int	   DSK_loadDiskCopy(char *, disk_struct **, FILE *);
   int	   DSK_readUnivImageHeader(FILE *, image_header *);
   int	   DSK_writeUnivImageHeader(FILE *, image_header *);

#endif
