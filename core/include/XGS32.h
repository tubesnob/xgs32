/* this ALWAYS GENERATED file contains the definitions for the interfaces */


/* File created by MIDL compiler version 5.01.0164 */
/* at Sat Jul 21 12:52:26 2001
 */
/* Compiler settings for E:\source\xgs32\core\XGS32.idl:
    Oicf (OptLev=i2), W1, Zp8, env=Win32, ms_ext, c_ext
    error checks: allocation ref bounds_check enum stub_data 
*/
//@@MIDL_FILE_HEADING(  )


/* verify that the <rpcndr.h> version is high enough to compile this file*/
#ifndef __REQUIRED_RPCNDR_H_VERSION__
#define __REQUIRED_RPCNDR_H_VERSION__ 440
#endif

#include "rpc.h"
#include "rpcndr.h"

#ifndef __RPCNDR_H_VERSION__
#error this stub requires an updated version of <rpcndr.h>
#endif // __RPCNDR_H_VERSION__

#ifndef COM_NO_WINDOWS_H
#include "windows.h"
#include "ole2.h"
#endif /*COM_NO_WINDOWS_H*/

#ifndef __XGS32_h__
#define __XGS32_h__

#ifdef __cplusplus
extern "C"{
#endif 

/* Forward Declarations */ 

#ifndef __IXGS32Obj_FWD_DEFINED__
#define __IXGS32Obj_FWD_DEFINED__
typedef interface IXGS32Obj IXGS32Obj;
#endif 	/* __IXGS32Obj_FWD_DEFINED__ */


#ifndef ___IXGS32ObjEvents_FWD_DEFINED__
#define ___IXGS32ObjEvents_FWD_DEFINED__
typedef interface _IXGS32ObjEvents _IXGS32ObjEvents;
#endif 	/* ___IXGS32ObjEvents_FWD_DEFINED__ */


#ifndef __XGS32Obj_FWD_DEFINED__
#define __XGS32Obj_FWD_DEFINED__

#ifdef __cplusplus
typedef class XGS32Obj XGS32Obj;
#else
typedef struct XGS32Obj XGS32Obj;
#endif /* __cplusplus */

#endif 	/* __XGS32Obj_FWD_DEFINED__ */


/* header files for imported files */
#include "oaidl.h"
#include "ocidl.h"

void __RPC_FAR * __RPC_USER MIDL_user_allocate(size_t);
void __RPC_USER MIDL_user_free( void __RPC_FAR * ); 

#ifndef __IXGS32Obj_INTERFACE_DEFINED__
#define __IXGS32Obj_INTERFACE_DEFINED__

/* interface IXGS32Obj */
/* [unique][helpstring][uuid][object] */ 


EXTERN_C const IID IID_IXGS32Obj;

#if defined(__cplusplus) && !defined(CINTERFACE)
    
    MIDL_INTERFACE("F15833FE-914D-11D3-A08D-0090278C0EC7")
    IXGS32Obj : public IUnknown
    {
    public:
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Initialize( void) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE Shutdown( void) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EmulatorRun( void) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EmulatorStop( void) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_RunState( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_RAMSize( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_RAMSize( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ROMFile( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_ROMFile( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_Font40File( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_Font40File( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_Font80File( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_Font80File( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_BRAMFile( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_BRAMFile( 
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_SmartPort( 
            /* [in] */ long lPortNum,
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_SmartPort( 
            /* [in] */ long lPortNum,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_Slot( 
            /* [in] */ long lSlotNum,
            /* [in] */ long lDeviceNum,
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_Slot( 
            /* [in] */ long lSlotNum,
            /* [in] */ long lDeviceNum,
            /* [in] */ BSTR newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_AverageRunSpeed( 
            /* [retval][out] */ double __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_TargetRunSpeed( 
            /* [retval][out] */ double __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_TargetRunSpeed( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_AttachedJoySticks( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_ValidDisplayModes( 
            /* [retval][out] */ VARIANT __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_TargetVideoRefreshRate( 
            /* [retval][out] */ double __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_TargetVideoRefreshRate( 
            /* [in] */ double newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_AverageVideoRefreshRate( 
            /* [retval][out] */ double __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_LockVideoUpdates( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_LockVideoUpdates( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE SetVideoMode( 
            /* [in] */ long lX,
            /* [in] */ long lY,
            /* [in] */ long lBPP,
            /* [in] */ long lRefreshRate,
            /* [in] */ long lWindowed,
            /* [in] */ long lWindowedParentHwnd) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_SoundMode( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_SoundMode( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_SoundOutputBuffers( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_SoundOutputBuffers( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_SoundOutputBufferSize( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_SoundOutputBufferSize( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_DebugMode( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_DebugMode( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_Version( 
            /* [retval][out] */ BSTR __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_EmulatorUpdatePeriod( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_EmulatorUpdatePeriod( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_EmulatorUpdateMode( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_EmulatorUpdateMode( 
            /* [in] */ long newVal) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_LocalSpeedIndex( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring] */ HRESULT STDMETHODCALLTYPE EmulatorPause( void) = 0;
        
        virtual /* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE get_JoystickMode( 
            /* [retval][out] */ long __RPC_FAR *pVal) = 0;
        
        virtual /* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE put_JoystickMode( 
            /* [in] */ long newVal) = 0;
        
    };
    
#else 	/* C style interface */

    typedef struct IXGS32ObjVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            IXGS32Obj __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            IXGS32Obj __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Initialize )( 
            IXGS32Obj __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Shutdown )( 
            IXGS32Obj __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *EmulatorRun )( 
            IXGS32Obj __RPC_FAR * This);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *EmulatorStop )( 
            IXGS32Obj __RPC_FAR * This);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_RunState )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_RAMSize )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_RAMSize )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ROMFile )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_ROMFile )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Font40File )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_Font40File )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Font80File )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_Font80File )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_BRAMFile )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_BRAMFile )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_SmartPort )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ long lPortNum,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_SmartPort )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ long lPortNum,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Slot )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ long lSlotNum,
            /* [in] */ long lDeviceNum,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_Slot )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ long lSlotNum,
            /* [in] */ long lDeviceNum,
            /* [in] */ BSTR newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_AverageRunSpeed )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ double __RPC_FAR *pVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TargetRunSpeed )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ double __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_TargetRunSpeed )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ double newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_AttachedJoySticks )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_ValidDisplayModes )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ VARIANT __RPC_FAR *pVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_TargetVideoRefreshRate )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ double __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_TargetVideoRefreshRate )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ double newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_AverageVideoRefreshRate )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ double __RPC_FAR *pVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_LockVideoUpdates )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_LockVideoUpdates )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *SetVideoMode )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ long lX,
            /* [in] */ long lY,
            /* [in] */ long lBPP,
            /* [in] */ long lRefreshRate,
            /* [in] */ long lWindowed,
            /* [in] */ long lWindowedParentHwnd);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_SoundMode )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_SoundMode )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_SoundOutputBuffers )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_SoundOutputBuffers )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_SoundOutputBufferSize )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_SoundOutputBufferSize )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_DebugMode )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_DebugMode )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_Version )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ BSTR __RPC_FAR *pVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_EmulatorUpdatePeriod )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_EmulatorUpdatePeriod )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_EmulatorUpdateMode )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_EmulatorUpdateMode )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_LocalSpeedIndex )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *EmulatorPause )( 
            IXGS32Obj __RPC_FAR * This);
        
        /* [helpstring][propget] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *get_JoystickMode )( 
            IXGS32Obj __RPC_FAR * This,
            /* [retval][out] */ long __RPC_FAR *pVal);
        
        /* [helpstring][propput] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *put_JoystickMode )( 
            IXGS32Obj __RPC_FAR * This,
            /* [in] */ long newVal);
        
        END_INTERFACE
    } IXGS32ObjVtbl;

    interface IXGS32Obj
    {
        CONST_VTBL struct IXGS32ObjVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define IXGS32Obj_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define IXGS32Obj_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define IXGS32Obj_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define IXGS32Obj_Initialize(This)	\
    (This)->lpVtbl -> Initialize(This)

#define IXGS32Obj_Shutdown(This)	\
    (This)->lpVtbl -> Shutdown(This)

#define IXGS32Obj_EmulatorRun(This)	\
    (This)->lpVtbl -> EmulatorRun(This)

#define IXGS32Obj_EmulatorStop(This)	\
    (This)->lpVtbl -> EmulatorStop(This)

#define IXGS32Obj_get_RunState(This,pVal)	\
    (This)->lpVtbl -> get_RunState(This,pVal)

#define IXGS32Obj_get_RAMSize(This,pVal)	\
    (This)->lpVtbl -> get_RAMSize(This,pVal)

#define IXGS32Obj_put_RAMSize(This,newVal)	\
    (This)->lpVtbl -> put_RAMSize(This,newVal)

#define IXGS32Obj_get_ROMFile(This,pVal)	\
    (This)->lpVtbl -> get_ROMFile(This,pVal)

#define IXGS32Obj_put_ROMFile(This,newVal)	\
    (This)->lpVtbl -> put_ROMFile(This,newVal)

#define IXGS32Obj_get_Font40File(This,pVal)	\
    (This)->lpVtbl -> get_Font40File(This,pVal)

#define IXGS32Obj_put_Font40File(This,newVal)	\
    (This)->lpVtbl -> put_Font40File(This,newVal)

#define IXGS32Obj_get_Font80File(This,pVal)	\
    (This)->lpVtbl -> get_Font80File(This,pVal)

#define IXGS32Obj_put_Font80File(This,newVal)	\
    (This)->lpVtbl -> put_Font80File(This,newVal)

#define IXGS32Obj_get_BRAMFile(This,pVal)	\
    (This)->lpVtbl -> get_BRAMFile(This,pVal)

#define IXGS32Obj_put_BRAMFile(This,newVal)	\
    (This)->lpVtbl -> put_BRAMFile(This,newVal)

#define IXGS32Obj_get_SmartPort(This,lPortNum,pVal)	\
    (This)->lpVtbl -> get_SmartPort(This,lPortNum,pVal)

#define IXGS32Obj_put_SmartPort(This,lPortNum,newVal)	\
    (This)->lpVtbl -> put_SmartPort(This,lPortNum,newVal)

#define IXGS32Obj_get_Slot(This,lSlotNum,lDeviceNum,pVal)	\
    (This)->lpVtbl -> get_Slot(This,lSlotNum,lDeviceNum,pVal)

#define IXGS32Obj_put_Slot(This,lSlotNum,lDeviceNum,newVal)	\
    (This)->lpVtbl -> put_Slot(This,lSlotNum,lDeviceNum,newVal)

#define IXGS32Obj_get_AverageRunSpeed(This,pVal)	\
    (This)->lpVtbl -> get_AverageRunSpeed(This,pVal)

#define IXGS32Obj_get_TargetRunSpeed(This,pVal)	\
    (This)->lpVtbl -> get_TargetRunSpeed(This,pVal)

#define IXGS32Obj_put_TargetRunSpeed(This,newVal)	\
    (This)->lpVtbl -> put_TargetRunSpeed(This,newVal)

#define IXGS32Obj_get_AttachedJoySticks(This,pVal)	\
    (This)->lpVtbl -> get_AttachedJoySticks(This,pVal)

#define IXGS32Obj_get_ValidDisplayModes(This,pVal)	\
    (This)->lpVtbl -> get_ValidDisplayModes(This,pVal)

#define IXGS32Obj_get_TargetVideoRefreshRate(This,pVal)	\
    (This)->lpVtbl -> get_TargetVideoRefreshRate(This,pVal)

#define IXGS32Obj_put_TargetVideoRefreshRate(This,newVal)	\
    (This)->lpVtbl -> put_TargetVideoRefreshRate(This,newVal)

#define IXGS32Obj_get_AverageVideoRefreshRate(This,pVal)	\
    (This)->lpVtbl -> get_AverageVideoRefreshRate(This,pVal)

#define IXGS32Obj_get_LockVideoUpdates(This,pVal)	\
    (This)->lpVtbl -> get_LockVideoUpdates(This,pVal)

#define IXGS32Obj_put_LockVideoUpdates(This,newVal)	\
    (This)->lpVtbl -> put_LockVideoUpdates(This,newVal)

#define IXGS32Obj_SetVideoMode(This,lX,lY,lBPP,lRefreshRate,lWindowed,lWindowedParentHwnd)	\
    (This)->lpVtbl -> SetVideoMode(This,lX,lY,lBPP,lRefreshRate,lWindowed,lWindowedParentHwnd)

#define IXGS32Obj_get_SoundMode(This,pVal)	\
    (This)->lpVtbl -> get_SoundMode(This,pVal)

#define IXGS32Obj_put_SoundMode(This,newVal)	\
    (This)->lpVtbl -> put_SoundMode(This,newVal)

#define IXGS32Obj_get_SoundOutputBuffers(This,pVal)	\
    (This)->lpVtbl -> get_SoundOutputBuffers(This,pVal)

#define IXGS32Obj_put_SoundOutputBuffers(This,newVal)	\
    (This)->lpVtbl -> put_SoundOutputBuffers(This,newVal)

#define IXGS32Obj_get_SoundOutputBufferSize(This,pVal)	\
    (This)->lpVtbl -> get_SoundOutputBufferSize(This,pVal)

#define IXGS32Obj_put_SoundOutputBufferSize(This,newVal)	\
    (This)->lpVtbl -> put_SoundOutputBufferSize(This,newVal)

#define IXGS32Obj_get_DebugMode(This,pVal)	\
    (This)->lpVtbl -> get_DebugMode(This,pVal)

#define IXGS32Obj_put_DebugMode(This,newVal)	\
    (This)->lpVtbl -> put_DebugMode(This,newVal)

#define IXGS32Obj_get_Version(This,pVal)	\
    (This)->lpVtbl -> get_Version(This,pVal)

#define IXGS32Obj_get_EmulatorUpdatePeriod(This,pVal)	\
    (This)->lpVtbl -> get_EmulatorUpdatePeriod(This,pVal)

#define IXGS32Obj_put_EmulatorUpdatePeriod(This,newVal)	\
    (This)->lpVtbl -> put_EmulatorUpdatePeriod(This,newVal)

#define IXGS32Obj_get_EmulatorUpdateMode(This,pVal)	\
    (This)->lpVtbl -> get_EmulatorUpdateMode(This,pVal)

#define IXGS32Obj_put_EmulatorUpdateMode(This,newVal)	\
    (This)->lpVtbl -> put_EmulatorUpdateMode(This,newVal)

#define IXGS32Obj_get_LocalSpeedIndex(This,pVal)	\
    (This)->lpVtbl -> get_LocalSpeedIndex(This,pVal)

#define IXGS32Obj_EmulatorPause(This)	\
    (This)->lpVtbl -> EmulatorPause(This)

#define IXGS32Obj_get_JoystickMode(This,pVal)	\
    (This)->lpVtbl -> get_JoystickMode(This,pVal)

#define IXGS32Obj_put_JoystickMode(This,newVal)	\
    (This)->lpVtbl -> put_JoystickMode(This,newVal)

#endif /* COBJMACROS */


#endif 	/* C style interface */



/* [helpstring] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_Initialize_Proxy( 
    IXGS32Obj __RPC_FAR * This);


void __RPC_STUB IXGS32Obj_Initialize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_Shutdown_Proxy( 
    IXGS32Obj __RPC_FAR * This);


void __RPC_STUB IXGS32Obj_Shutdown_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_EmulatorRun_Proxy( 
    IXGS32Obj __RPC_FAR * This);


void __RPC_STUB IXGS32Obj_EmulatorRun_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_EmulatorStop_Proxy( 
    IXGS32Obj __RPC_FAR * This);


void __RPC_STUB IXGS32Obj_EmulatorStop_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_RunState_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_RunState_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_RAMSize_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_RAMSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_RAMSize_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB IXGS32Obj_put_RAMSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_ROMFile_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_ROMFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_ROMFile_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IXGS32Obj_put_ROMFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_Font40File_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_Font40File_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_Font40File_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IXGS32Obj_put_Font40File_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_Font80File_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_Font80File_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_Font80File_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IXGS32Obj_put_Font80File_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_BRAMFile_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_BRAMFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_BRAMFile_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ BSTR newVal);


void __RPC_STUB IXGS32Obj_put_BRAMFile_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_SmartPort_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ long lPortNum,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_SmartPort_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_SmartPort_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ long lPortNum,
    /* [in] */ BSTR newVal);


void __RPC_STUB IXGS32Obj_put_SmartPort_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_Slot_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ long lSlotNum,
    /* [in] */ long lDeviceNum,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_Slot_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_Slot_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ long lSlotNum,
    /* [in] */ long lDeviceNum,
    /* [in] */ BSTR newVal);


void __RPC_STUB IXGS32Obj_put_Slot_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_AverageRunSpeed_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ double __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_AverageRunSpeed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_TargetRunSpeed_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ double __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_TargetRunSpeed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_TargetRunSpeed_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ double newVal);


void __RPC_STUB IXGS32Obj_put_TargetRunSpeed_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_AttachedJoySticks_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_AttachedJoySticks_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_ValidDisplayModes_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ VARIANT __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_ValidDisplayModes_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_TargetVideoRefreshRate_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ double __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_TargetVideoRefreshRate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_TargetVideoRefreshRate_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ double newVal);


void __RPC_STUB IXGS32Obj_put_TargetVideoRefreshRate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_AverageVideoRefreshRate_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ double __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_AverageVideoRefreshRate_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_LockVideoUpdates_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_LockVideoUpdates_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_LockVideoUpdates_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB IXGS32Obj_put_LockVideoUpdates_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_SetVideoMode_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ long lX,
    /* [in] */ long lY,
    /* [in] */ long lBPP,
    /* [in] */ long lRefreshRate,
    /* [in] */ long lWindowed,
    /* [in] */ long lWindowedParentHwnd);


void __RPC_STUB IXGS32Obj_SetVideoMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_SoundMode_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_SoundMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_SoundMode_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB IXGS32Obj_put_SoundMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_SoundOutputBuffers_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_SoundOutputBuffers_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_SoundOutputBuffers_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB IXGS32Obj_put_SoundOutputBuffers_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_SoundOutputBufferSize_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_SoundOutputBufferSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_SoundOutputBufferSize_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB IXGS32Obj_put_SoundOutputBufferSize_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_DebugMode_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_DebugMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_DebugMode_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB IXGS32Obj_put_DebugMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_Version_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ BSTR __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_Version_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_EmulatorUpdatePeriod_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_EmulatorUpdatePeriod_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_EmulatorUpdatePeriod_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB IXGS32Obj_put_EmulatorUpdatePeriod_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_EmulatorUpdateMode_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_EmulatorUpdateMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_EmulatorUpdateMode_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB IXGS32Obj_put_EmulatorUpdateMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_LocalSpeedIndex_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_LocalSpeedIndex_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_EmulatorPause_Proxy( 
    IXGS32Obj __RPC_FAR * This);


void __RPC_STUB IXGS32Obj_EmulatorPause_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propget] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_get_JoystickMode_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [retval][out] */ long __RPC_FAR *pVal);


void __RPC_STUB IXGS32Obj_get_JoystickMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);


/* [helpstring][propput] */ HRESULT STDMETHODCALLTYPE IXGS32Obj_put_JoystickMode_Proxy( 
    IXGS32Obj __RPC_FAR * This,
    /* [in] */ long newVal);


void __RPC_STUB IXGS32Obj_put_JoystickMode_Stub(
    IRpcStubBuffer *This,
    IRpcChannelBuffer *_pRpcChannelBuffer,
    PRPC_MESSAGE _pRpcMessage,
    DWORD *_pdwStubPhase);



#endif 	/* __IXGS32Obj_INTERFACE_DEFINED__ */



#ifndef __XGS32Lib_LIBRARY_DEFINED__
#define __XGS32Lib_LIBRARY_DEFINED__

/* library XGS32Lib */
/* [helpstring][version][uuid] */ 


EXTERN_C const IID LIBID_XGS32Lib;

#ifndef ___IXGS32ObjEvents_DISPINTERFACE_DEFINED__
#define ___IXGS32ObjEvents_DISPINTERFACE_DEFINED__

/* dispinterface _IXGS32ObjEvents */
/* [helpstring][uuid] */ 


EXTERN_C const IID DIID__IXGS32ObjEvents;

#if defined(__cplusplus) && !defined(CINTERFACE)

    MIDL_INTERFACE("F1583400-914D-11D3-A08D-0090278C0EC7")
    _IXGS32ObjEvents : public IDispatch
    {
    };
    
#else 	/* C style interface */

    typedef struct _IXGS32ObjEventsVtbl
    {
        BEGIN_INTERFACE
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *QueryInterface )( 
            _IXGS32ObjEvents __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [iid_is][out] */ void __RPC_FAR *__RPC_FAR *ppvObject);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *AddRef )( 
            _IXGS32ObjEvents __RPC_FAR * This);
        
        ULONG ( STDMETHODCALLTYPE __RPC_FAR *Release )( 
            _IXGS32ObjEvents __RPC_FAR * This);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfoCount )( 
            _IXGS32ObjEvents __RPC_FAR * This,
            /* [out] */ UINT __RPC_FAR *pctinfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetTypeInfo )( 
            _IXGS32ObjEvents __RPC_FAR * This,
            /* [in] */ UINT iTInfo,
            /* [in] */ LCID lcid,
            /* [out] */ ITypeInfo __RPC_FAR *__RPC_FAR *ppTInfo);
        
        HRESULT ( STDMETHODCALLTYPE __RPC_FAR *GetIDsOfNames )( 
            _IXGS32ObjEvents __RPC_FAR * This,
            /* [in] */ REFIID riid,
            /* [size_is][in] */ LPOLESTR __RPC_FAR *rgszNames,
            /* [in] */ UINT cNames,
            /* [in] */ LCID lcid,
            /* [size_is][out] */ DISPID __RPC_FAR *rgDispId);
        
        /* [local] */ HRESULT ( STDMETHODCALLTYPE __RPC_FAR *Invoke )( 
            _IXGS32ObjEvents __RPC_FAR * This,
            /* [in] */ DISPID dispIdMember,
            /* [in] */ REFIID riid,
            /* [in] */ LCID lcid,
            /* [in] */ WORD wFlags,
            /* [out][in] */ DISPPARAMS __RPC_FAR *pDispParams,
            /* [out] */ VARIANT __RPC_FAR *pVarResult,
            /* [out] */ EXCEPINFO __RPC_FAR *pExcepInfo,
            /* [out] */ UINT __RPC_FAR *puArgErr);
        
        END_INTERFACE
    } _IXGS32ObjEventsVtbl;

    interface _IXGS32ObjEvents
    {
        CONST_VTBL struct _IXGS32ObjEventsVtbl __RPC_FAR *lpVtbl;
    };

    

#ifdef COBJMACROS


#define _IXGS32ObjEvents_QueryInterface(This,riid,ppvObject)	\
    (This)->lpVtbl -> QueryInterface(This,riid,ppvObject)

#define _IXGS32ObjEvents_AddRef(This)	\
    (This)->lpVtbl -> AddRef(This)

#define _IXGS32ObjEvents_Release(This)	\
    (This)->lpVtbl -> Release(This)


#define _IXGS32ObjEvents_GetTypeInfoCount(This,pctinfo)	\
    (This)->lpVtbl -> GetTypeInfoCount(This,pctinfo)

#define _IXGS32ObjEvents_GetTypeInfo(This,iTInfo,lcid,ppTInfo)	\
    (This)->lpVtbl -> GetTypeInfo(This,iTInfo,lcid,ppTInfo)

#define _IXGS32ObjEvents_GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)	\
    (This)->lpVtbl -> GetIDsOfNames(This,riid,rgszNames,cNames,lcid,rgDispId)

#define _IXGS32ObjEvents_Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)	\
    (This)->lpVtbl -> Invoke(This,dispIdMember,riid,lcid,wFlags,pDispParams,pVarResult,pExcepInfo,puArgErr)

#endif /* COBJMACROS */


#endif 	/* C style interface */


#endif 	/* ___IXGS32ObjEvents_DISPINTERFACE_DEFINED__ */


EXTERN_C const CLSID CLSID_XGS32Obj;

#ifdef __cplusplus

class DECLSPEC_UUID("F15833FF-914D-11D3-A08D-0090278C0EC7")
XGS32Obj;
#endif
#endif /* __XGS32Lib_LIBRARY_DEFINED__ */

/* Additional Prototypes for ALL interfaces */

unsigned long             __RPC_USER  BSTR_UserSize(     unsigned long __RPC_FAR *, unsigned long            , BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  BSTR_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, BSTR __RPC_FAR * ); 
void                      __RPC_USER  BSTR_UserFree(     unsigned long __RPC_FAR *, BSTR __RPC_FAR * ); 

unsigned long             __RPC_USER  VARIANT_UserSize(     unsigned long __RPC_FAR *, unsigned long            , VARIANT __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  VARIANT_UserMarshal(  unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, VARIANT __RPC_FAR * ); 
unsigned char __RPC_FAR * __RPC_USER  VARIANT_UserUnmarshal(unsigned long __RPC_FAR *, unsigned char __RPC_FAR *, VARIANT __RPC_FAR * ); 
void                      __RPC_USER  VARIANT_UserFree(     unsigned long __RPC_FAR *, VARIANT __RPC_FAR * ); 

/* end of Additional Prototypes */

#ifdef __cplusplus
}
#endif

#endif
