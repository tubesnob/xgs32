#ifndef __XGSSTATE__
#define __XGSSTATE__

   #include "xgstypes.h"

	extern   HINSTANCE g_hDLLInstance;

   extern   long  g_lVidWindowed;
   extern   long  g_lVidWidth;
   extern   long  g_lVidHeight;
   extern   long  g_lVidBitDepth;
   extern   long  g_lVidRefreshRate;
   extern   long  g_lEmulatorSignal;
   extern	HWND	g_hVidHWND;
   extern   long  g_lSoundOutputBufferSize;
   extern   long  g_lSoundOutputBuffers;
   extern   long  g_lShowStatus;
   extern   long  g_lDebugMode;
   extern   long  g_lSoundMode;
   extern	long	g_lNativeCPUSpeed;
   extern   long  g_lThrottleSleepTime;
   extern   FILE  *g_pDebugFile;
   extern   long  g_lRAMSize;
   extern   BSTR  g_bstrROMFile;
   extern   BSTR  g_bstrFont40File;
   extern   BSTR  g_bstrFont80File;
   extern   BSTR  g_bstrBRAMFile;
   extern   BSTR  g_bstrSmartPort[NUM_SMPT_DEVS];
   extern   BSTR  g_bstrSlot[8][3];
   extern   double   g_dTargetRunSpeed;
   extern	double	g_dAverageRunSpeed;
   extern   long     g_lKeyboardRepeatRate;
   extern   long     g_lKeyboardRepeatDelay;
   extern   double   g_dTargetVideoRefreshRate;
   extern   double   g_dAverageVideoRefreshRate;
   extern   char     g_pszDebugString[STRING_BUFFER_SIZE];
   extern   long     g_lLockVideoUpdates;
   extern   long     g_lEmulatorUpdatePeriod;
   extern   long     g_lEmulatorUpdateMode;
   extern   long     g_lJoystickMode;

   char *ConvertBSTR(BSTR bstrTemp);
   void STATE_Initialize();
   void STATE_Persist(char *output_file);
   void STATE_Load(char *input_file);
   void STATE_Shutdown();
   void STATE_InitDebug();
   void STATE_StopDebug();
   void DoErrorMessage(char *msg, int flag=0);

#endif