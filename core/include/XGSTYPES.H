
#ifndef __XGSTYPES__
#define __XGSTYPES__

   #include "xgsdefs.h"

   #include <sys/types.h>
   #include <stdio.h>

   typedef unsigned  char	byte;
   typedef signed    char	sbyte;
   typedef unsigned long	word32;
   typedef signed long	   sword32;
   typedef unsigned short	word16;
   typedef signed short	   sword16;
   typedef signed char	   offset_s;
   typedef signed short	   offset_l;

typedef struct tagRGBQUAD {
  byte rgbBlue;
  byte rgbGreen;
  byte rgbRed;
  byte rgbReserved;
} RGBQUAD;

typedef word32 HANDLE;
typedef char* HPSTR;
typedef word32 HGLOBAL;

typedef struct tagWAVEHDR
{
  word32 *lpData;
  word32 dwBytesRecorded;
  word32 dwUser;
  word32 dwFlags;
  word32 dwLoops;
  tagWAVEHDR *lpNext;
  word32 reserved;
} *LPWAVEHDR;

   typedef union 
   {
	   struct { byte	L,H; } B;
	   word16	W;
   } dualw;

   typedef union 
   {
	   struct { byte	L,H,B,Z; } B;
	   struct { word16	L,H; } W;
	   word32	A;
   } duala;


   typedef union 
   {		
	   struct { byte L,H,PB,Z; } B;
	   struct { word16 PC,Z; } W;
	   word32	A;
   } tPC;

   typedef  union 
   {
   	byte		B[4];
   	time_t		L;
   } tCLKCURRTIME;

   typedef struct 
   {
	   byte	*readPtr;
	   byte	*writePtr;
	   word16	readFlags;
	   word16	writeFlags;
	   word16	extraFlags1;
	   word16	extraFlags2;
   } mem_pagestruct;

   /* Struct for SKI command processing. Command begins execution after we	*/
   /* read bytes_to_read bytes, and ends after bytes_to_write bytes are	*/
   /* written. Note read = get from user, write = send to user.		*/
   typedef struct 
   {
	   int	command;		/* command byte */
	   int	bytes_to_read;		/* bytes command will read */
	   int	bytes_to_write;		/* bytes command will write */
   } ski_command;

   typedef struct 
   {
	   byte	cmd;
	   byte	parms;
	   void	(*func)(int);
   } smpt_cmd_def;

   typedef struct 
   {
	   char	magic[4];	/* "2IMG"                                  */
	   char	creator[4];	/* Creator signature                       */
	   word16	header_len;	/* Length of header in bytes            k  */
	   word16	version;	/* Image version                           */
	   word32	image_format;	/* Image data format (see below)           */
	   word32	flags;		/* Format flags (see below)                */
	   word32	num_blocks;	/* Number of 512-byte blocks in this image */
	   word32	data_offset;	/* File offset to start of data            */
	   word32	data_len;	/* Length of data in bytes                 */
	   word32	cmnt_offset;	/* File offset to start of comment         */
	   word32	cmnt_len;	/* Length of comment in bytes              */
	   word32	creator_offset;	/* File offset to start of creator data    */
	   word32	creator_len;	/* Length of creator data in bytes         */
	   word32	spare[4];	/* Spare words (pads header to 64 bytes)   */
   } image_header;

   typedef struct 
   {
	   image_header	header;
	   int		image_type;
	   int		read_only;
	   char		*pathname;
	   FILE		*stream;
   } disk_struct;

   typedef struct 
   {
	   char	diskName[64];	/* Disk name, as a Pascal string	*/
	   word32	dataSize;	/* Size of block data in bytes		*/
	   word32	tagSize;	/* Size of tag information in bytes	*/
	   word32	dataChecksum;	/* Checksum of block data		*/
	   word32	tagChecksum;	/* Checksum of tag data			*/
	   char	diskFormat;	/* 0 = 400K, 1 = 800K, 2 = 720K,	*/
				   /* 3 = 1440K				*/
	   char	formatByte;	/* 0x12 = 400K, 0x22 = >400K Mac,	*/
				   /* 0x24 = 800k Apple II			*/
	   word16	reserved_private;	/* reserved				*/
   } diskcopy_header;

   typedef struct 
   {
	   int	track_valid;
	   int	track_dirty;
	   int	overflow_size;
	   int	track_len;
	   byte	*nib_area;
   } Track;

   typedef struct 
   {
	   disk_struct	*disk;
	   int		cur_qtr_track;
	   int		vol_num;
	   word32		time_last_read;
	   int		last_phase;
	   int		nib_pos;
	   Track		track[35*4];
   } Disk525;

   typedef struct 
   {
	   disk_struct	*disk;
	   int		motor_on;
	   int		cur_track;
	   int		disk_switched;
	   int		step;
	   int		head;
	   int		nib_pos;
	   Track		track[80*2];
   } Disk35;

   typedef struct 
   {
	   int	red,green,blue;
   } vid_color;

   typedef struct 
   {
	   int	left,right;
   } snd_sample_struct;

   typedef struct 
   {
	   HANDLE hData;
	   HPSTR lpData;
	   HGLOBAL hWaveHdr;
	   LPWAVEHDR lpWaveHdr;
	   int busy;
   } soundinfo;

   // the structure which holds pixel RGB masks
   typedef struct _RGBMASK16
   {
      unsigned long rgbRed; // red component
      unsigned long rgbGreen; // green component
      unsigned long rgbBlue; // blue component
   } tRGBMASK16;

   // the structure which holds screen surface info (5,6,5 or 5,5,5 and masking)
   typedef struct _RGB16DEF
   {   
      RGBQUAD     depth;   
      RGBQUAD     Amount;   
      RGBQUAD     Position;
      tRGBMASK16  Mask;
   } tRGB16DEF;




#endif



