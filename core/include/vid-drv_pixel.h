
#ifndef __VIDDRVPIXEL__
#define __VIDDRVPIXEL__

   #include "xgstypes.h"

   extern   void		(*VID_PixelPlot[10])(long,long,long);

   void VID_PP_32_SUPER(long x, long y, long lColorIndex);
   void VID_PP_32_BORDER(long x, long y, long lColorIndex);
   void VID_PP_32_TEXT(long x, long y, long lColorIndex);
   void VID_PP_32_HIRES(long x, long y, long lColorIndex);
   void VID_PP_32_DBL(long x, long y, long lColorIndex);
   void VID_PP_24_SUPER(long x, long y, long lColorIndex);
   void VID_PP_24_BORDER(long x, long y, long lColorIndex);
   void VID_PP_24_TEXT(long x, long y, long lColorIndex);
   void VID_PP_24_HIRES(long x, long y, long lColorIndex);
   void VID_PP_24_DBL(long x, long y, long lColorIndex);
   void VID_PP_16_SUPER(long x, long y, long lColorIndex);
   void VID_PP_16_BORDER(long x, long y, long lColorIndex);
   void VID_PP_16_TEXT(long x, long y, long lColorIndex);
   void VID_PP_16_HIRES(long x, long y, long lColorIndex);
   void VID_PP_16_DBL(long x, long y, long lColorIndex);
   void VID_PP_8_SUPER(long x, long y, long lColorIndex);
   void VID_PP_8_BORDER(long x, long y, long lColorIndex);
   void VID_PP_8_TEXT(long x, long y, long lColorIndex);
   void VID_PP_8_HIRES(long x, long y, long lColorIndex);
   void VID_PP_8_DBL(long x, long y, long lColorIndex);


#endif