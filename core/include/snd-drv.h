#ifndef __SNDDRV__
#define __SNDDRV__

   #include "xgstypes.h"

   extern   soundinfo   mybuffers[MAXSOUNDBUFFERS];

   int      SND_outputInit(int);
   void     SND_outputShutdown(void);
   size_t   SND_outputWrite(snd_sample_struct *, size_t);
   void     SND_PauseSound();
   void     SND_RestoreSoundAfterPause();

#endif
