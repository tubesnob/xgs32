/*

   COPYRIGHT NOTIFICATION

   This information is copyrighted by
   Steven W. Mentzer

   You may not sell or assume ownership of this information 
   without my express written consent.

   This information can be freely redistributed, published, compiled, used or modified.

   Inquiries should be directed to   steve@staticmonkey.com

*/

// XGS32Obj.cpp : Implementation of CXGS32Obj
#include "stdafx.h"
#include "XGS32.h"
#include "XGS32Obj.h"

#include "xgstypes.h"
#include "xgsstate.h"
#include "smtport.h"
#include "iwm.h"
#include "emul.h"
#include "vid-drv.h"
#include "adb-drv.h"

/////////////////////////////////////////////////////////////////////////////
// CXGS32Obj

STDMETHODIMP CXGS32Obj::InterfaceSupportsErrorInfo(REFIID riid)
{
	static const IID* arr[] = 
	{
		&IID_IXGS32Obj
	};
	for (int i=0; i < sizeof(arr) / sizeof(arr[0]); i++)
	{
		if (InlineIsEqualGUID(*arr[i],riid))
			return S_OK;
	}
	return S_FALSE;
}

STDMETHODIMP CXGS32Obj::get_RAMSize(long *pVal)
{
   *pVal = g_lRAMSize;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_RAMSize(long newVal)
{
   g_lRAMSize = newVal;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_ROMFile(BSTR *pVal)
{
   if (g_bstrROMFile)
      *pVal = SysAllocString(g_bstrROMFile);
   else
      *pVal = NULL;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_ROMFile(BSTR newVal)
{
   if (g_bstrROMFile)
      SysFreeString(g_bstrROMFile);
   if (newVal)
      g_bstrROMFile = SysAllocString(newVal);
   else
      g_bstrROMFile = NULL;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_Font40File(BSTR *pVal)
{
   if (g_bstrFont40File)
      *pVal = SysAllocString(g_bstrFont40File);
   else
      *pVal = NULL;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_Font40File(BSTR newVal)
{
   if (g_bstrFont40File)
      SysFreeString(g_bstrFont40File);
   if (newVal)
      g_bstrFont40File = SysAllocString(newVal);
   else
      g_bstrFont40File = NULL;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_Font80File(BSTR *pVal)
{
   if (g_bstrFont80File)
      *pVal = SysAllocString(g_bstrFont80File);
   else
      *pVal = NULL;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_Font80File(BSTR newVal)
{
   if (g_bstrFont80File)
      SysFreeString(g_bstrFont80File);
   if (newVal)
      g_bstrFont80File = SysAllocString(newVal);
   else
      g_bstrFont80File = NULL;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_BRAMFile(BSTR *pVal)
{
   if (g_bstrBRAMFile)
      *pVal = SysAllocString(g_bstrBRAMFile);
   else
      *pVal = NULL;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_BRAMFile(BSTR newVal)
{
   if (g_bstrBRAMFile)
      SysFreeString(g_bstrBRAMFile);
   if (newVal) 
      g_bstrBRAMFile = SysAllocString(newVal);
   else
      g_bstrBRAMFile = NULL;
	return S_OK;
}


STDMETHODIMP CXGS32Obj::get_SmartPort(long lPortNum, BSTR *pVal)
{
   if ((lPortNum >=0) && (lPortNum<NUM_SMPT_DEVS))
   {
      if (g_bstrSmartPort[lPortNum])
         *pVal = SysAllocString(g_bstrSmartPort[lPortNum]);
      else
         *pVal = NULL;
      return S_OK;
   }
   else
      return E_INVALIDARG;
}

STDMETHODIMP CXGS32Obj::put_SmartPort(long lPortNum, BSTR newVal)
{
   if ((lPortNum >=0) && (lPortNum<NUM_SMPT_DEVS))
   {
      if (g_bstrSmartPort[lPortNum])
         SysFreeString(g_bstrSmartPort[lPortNum]);
      if (newVal)
      {
         g_bstrSmartPort[lPortNum] = SysAllocString(newVal);
         if ((g_lEmulatorSignal==EMUL_RUNNING) || (g_lEmulatorSignal==EMUL_PAUSED))
         {
            SMPT_unloadDrive(lPortNum);
            SMPT_loadDrive(lPortNum,ConvertBSTR(g_bstrSmartPort[lPortNum]));
         };
      }
      else
      {
         g_bstrSmartPort[lPortNum] = NULL;
         if ((g_lEmulatorSignal==EMUL_RUNNING) || (g_lEmulatorSignal==EMUL_PAUSED))
            SMPT_unloadDrive(lPortNum);
      };
   	return S_OK;
   }
   else
      return E_INVALIDARG;
}

STDMETHODIMP CXGS32Obj::get_Slot(long lSlotNum, long lDeviceNum, BSTR *pVal)
{
   if ((lSlotNum >= 0) && (lSlotNum < 8) && (lDeviceNum >= 0) && (lDeviceNum <= 2))
   {
      if (g_bstrSlot[lSlotNum][lDeviceNum])
         *pVal = SysAllocString(g_bstrSlot[lSlotNum][lDeviceNum]);
      else
         *pVal = NULL;
      return S_OK;
   }
   else
      return E_INVALIDARG;
}

STDMETHODIMP CXGS32Obj::put_Slot(long lSlotNum, long lDeviceNum, BSTR newVal)
{
   if ((lSlotNum >= 0) && (lSlotNum < 8) && (lDeviceNum >= 0) && (lDeviceNum <= 2))
   {
      if (g_bstrSlot[lSlotNum][lDeviceNum])
         SysFreeString(g_bstrSlot[lSlotNum][lDeviceNum]);
      if (newVal)
      {
         g_bstrSlot[lSlotNum][lDeviceNum] = SysAllocString(newVal);
         if ((g_lEmulatorSignal==EMUL_RUNNING) || (g_lEmulatorSignal==EMUL_PAUSED))
         {
            IWM_unloadDrive(lSlotNum,lDeviceNum);
            IWM_loadDrive(lSlotNum,lDeviceNum,ConvertBSTR(g_bstrSlot[lSlotNum][lDeviceNum]));
         }
      }
      else
      {
         if ((g_lEmulatorSignal==EMUL_RUNNING) || (g_lEmulatorSignal==EMUL_PAUSED))
            IWM_unloadDrive(lSlotNum,lDeviceNum);
         g_bstrSlot[lSlotNum][lDeviceNum] = NULL;
      };
      return S_OK;
   }
   return E_INVALIDARG;
}

STDMETHODIMP CXGS32Obj::get_AverageRunSpeed(double *pVal)
{
	*pVal = g_dAverageRunSpeed;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_TargetRunSpeed(double *pVal)
{
   *pVal = g_dTargetRunSpeed;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_TargetRunSpeed(double newVal)
{
   g_dTargetRunSpeed = newVal;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_DebugMode(long *pVal)
{
   *pVal = g_lDebugMode;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_DebugMode(long newVal)
{
   g_lDebugMode = newVal;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::Initialize()
{
   EMUL_Initialize();   
	return S_OK;
}

STDMETHODIMP CXGS32Obj::Shutdown()
{
   if (g_lEmulatorSignal!= EMUL_IDLE)
   {
      EmulatorStop();
	   EMUL_Shutdown();
   };
	return S_OK;
}

STDMETHODIMP CXGS32Obj::EmulatorRun()
{
   EMUL_start();
	return S_OK;
}

STDMETHODIMP CXGS32Obj::EmulatorStop()
{
   EMUL_stop();
	return S_OK;
}


STDMETHODIMP CXGS32Obj::get_SoundMode(long *pVal)
{
	*pVal = g_lSoundMode;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_SoundMode(long newVal)
{
	g_lSoundMode = newVal;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_ValidDisplayModes(VARIANT *pVal)
{
   VID_GetDisplayModes(pVal);
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_AttachedJoySticks(VARIANT *pVal)
{
   ADB_GetAvailableJoySticks(pVal);
	return S_OK;
}

STDMETHODIMP CXGS32Obj::SetVideoMode(long lX, long lY, long lBPP, long lRefreshRate, long lWindowed, long lWindowedParentHwnd)
{
   g_lVidWidth=lX;
   g_lVidHeight = lY;
   g_lVidBitDepth = lBPP;
   g_lVidRefreshRate = lRefreshRate;
   g_lVidWindowed = lWindowed;
   g_lParentWindow = lWindowedParentHwnd;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_RunState(long *pVal)
{
   *pVal = g_lEmulatorSignal;
	return S_OK;
}


STDMETHODIMP CXGS32Obj::get_TargetVideoRefreshRate(double *pVal)
{
   *pVal = g_dTargetVideoRefreshRate;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_TargetVideoRefreshRate(double newVal)
{
   g_dTargetVideoRefreshRate = newVal;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_AverageVideoRefreshRate(double *pVal)
{
   *pVal = g_dAverageVideoRefreshRate;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_SoundOutputBuffers(long *pVal)
{
   *pVal = g_lSoundOutputBuffers;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_SoundOutputBuffers(long newVal)
{
   g_lSoundOutputBuffers = newVal;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_SoundOutputBufferSize(long *pVal)
{
   *pVal = g_lSoundOutputBufferSize;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_SoundOutputBufferSize(long newVal)
{
   g_lSoundOutputBufferSize = newVal;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_LockVideoUpdates(long *pVal)
{
   *pVal = g_lLockVideoUpdates;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_LockVideoUpdates(long newVal)
{
   g_lLockVideoUpdates=newVal;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_Version(BSTR *pVal)
{
   *pVal = SysAllocString(CComBSTR(XGS32_VERSION));
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_EmulatorUpdatePeriod(long *pVal)
{
	*pVal = g_lEmulatorUpdatePeriod;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_EmulatorUpdatePeriod(long newVal)
{
	g_lEmulatorUpdatePeriod = newVal;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_EmulatorUpdateMode(long *pVal)
{
	*pVal = g_lEmulatorUpdateMode;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_EmulatorUpdateMode(long newVal)
{
	g_lEmulatorUpdateMode = newVal;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_LocalSpeedIndex(long *pVal)
{
	long		lCycleCount=0;
	long		lStartTick;
	long		lEndTick;
   char     *pszTemp=NULL;
   char     *pszBuffer=NULL;
   long     i,j;

   // Perform a simple loop to determine how fast the computer is. We will execute the loop for no less
   // than 1024ms and increment a counter to determine how many times we actually do it..
	lEndTick=0;
	lStartTick=GetTickCount();
	while((lEndTick-lStartTick) < 1024)
	{

		lCycleCount++;
      goto branch3;

branch1:

            for (j=0; j<64; j++)
            {
               pszTemp = (char*) malloc(1024);  
               if (pszTemp)
               {
                  memset(pszTemp,0,1024);
                  pszBuffer = (char*) malloc(16);
                  if(pszBuffer) 
                  {
                     for (i=65; i < 128; i++)
                     {
                        memset(pszBuffer,0,16);
                        pszBuffer[0] = (char)i;                        
                        strcat(pszTemp,pszBuffer);
                     };
                     free(pszBuffer);
                     pszBuffer=NULL;
                  };
                  free(pszTemp);
                  pszTemp=NULL;
               }
               else
                  pszTemp=NULL;
            };
            goto endofloop;

branch2:
            lEndTick=GetTickCount();
            goto branch1;

branch3:

            lCycleCount++;
            lCycleCount--;
            goto branch2;

endofloop:
            lCycleCount++;
            lCycleCount--;
           		
	};
   *pVal = lCycleCount;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::EmulatorPause()
{
   EMUL_pause();
	return S_OK;
}

STDMETHODIMP CXGS32Obj::get_JoystickMode(long *pVal)
{
   *pVal=g_lJoystickMode;
	return S_OK;
}

STDMETHODIMP CXGS32Obj::put_JoystickMode(long newVal)
{
   g_lJoystickMode=newVal;
	return S_OK;
}
