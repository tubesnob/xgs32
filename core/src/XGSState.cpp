
#include "xgstypes.h"
#include "xgsstate.h"

long			g_lNativeCPUSpeed;
long        g_lThrottleSleepTime = 0;
long        g_lVidWidth=800;
long        g_lVidHeight=600;
long        g_lVidBitDepth=24;
long        g_lVidRefreshRate=0;
long			g_lVidWindowed=1;
HWND        g_hVidHWND=NULL;
long        g_lSoundOutputBufferSize=2800;
long        g_lSoundOutputBuffers=5;
long        g_lShowStatus;
FILE        *g_pDebugFile;
long        g_lEmulatorSignal=EMUL_IDLE;
long        g_lRAMSize;
BSTR        g_bstrROMFile;
BSTR        g_bstrFont40File;
BSTR        g_bstrFont80File;
BSTR        g_bstrBRAMFile;
BSTR        g_bstrSmartPort[NUM_SMPT_DEVS];
BSTR        g_bstrSlot[8][3];
long        g_lDebugMode=1;
long        g_lSoundMode;
double      g_dTargetRunSpeed;
double		g_dAverageRunSpeed;
long        g_lKeyboardRepeatRate;
long        g_lKeyboardRepeatDelay;
double      g_dTargetVideoRefreshRate;
double      g_dAverageVideoRefreshRate;
char        g_pszDebugString[STRING_BUFFER_SIZE];
long        g_lLockVideoUpdates=0;
long        g_lEmulatorUpdatePeriod=100;
long        g_lEmulatorUpdateMode=0;
long        g_lJoystickMode=0;


char *ConvertBSTR(BSTR bstrTemp)
{
   char     *rv = NULL;
   size_t   size;
   char     *buffer = NULL;

   buffer = (char*) malloc(STRING_BUFFER_SIZE);
   if (buffer) 
   {
      size = wcstombs(buffer,bstrTemp,STRING_BUFFER_SIZE);
      if (size ==  wcslen(bstrTemp))
      {
         buffer[size]=0;
         rv = buffer;
      }
      else
      {
         free(buffer);
         buffer = NULL;
      };
   };
   return (rv);
};

void STATE_Initialize()
{
   long i;
   g_lRAMSize = 2;
   g_bstrROMFile = NULL;
   g_bstrFont40File = NULL;
   g_bstrFont80File = NULL;
   g_bstrBRAMFile = NULL;
   g_lSoundMode = 0;
   g_dTargetRunSpeed = 3.0;
	g_dAverageRunSpeed = 0;
   g_dTargetVideoRefreshRate = 60;
   for (i=0; i < NUM_SMPT_DEVS; i++)
      g_bstrSmartPort[i] = NULL;
   for (i=0; i < 6; i++)
   {
      g_bstrSlot[i][0];
      g_bstrSlot[i][1];
   };
   g_pDebugFile = NULL;
   g_lKeyboardRepeatRate=30;
   g_lKeyboardRepeatDelay=500;

   g_lDebugMode = 1;


   STATE_InitDebug();
};

void STATE_Persist(char *output_file)
{

};

void STATE_Load(char *input_file)
{


};

void STATE_Shutdown()
{
   STATE_StopDebug();
};

void STATE_InitDebug()
{
   g_pDebugFile = fopen("xgs.dbg","wb");
};


void STATE_StopDebug()
{
   if (g_pDebugFile)
      fclose(g_pDebugFile);
   g_pDebugFile = NULL;
};

void DoErrorMessage(char *msg, int flag)
{
   char pszErrorMessage[2048];

   // if the user is running in full-screen mode, we don't want to bring up message boxes.
   // the user will never be able to see them, and it will prevent them from properly 
   // quitting the application. The solution would be to have the message boxes run in a seperate thread
   // but that would be quite overkill.. :)
   //
   // so, if flag is *1*, then we will always display the dialog box.
   // if flag is 0, we will only display the dialog box if the user is in windowed mode.
   if  ((!g_lVidWindowed) || (flag))
   {
      sprintf(pszErrorMessage,"An Error Was Encountered When Running XGS/32\n\n%s\n\nIf this error persists, please contact smentzer@pacbell.net for further assistance.",msg);
      MessageBox(NULL,pszErrorMessage,"XGS/32 Error",MB_ICONERROR|MB_OK);
   };

};